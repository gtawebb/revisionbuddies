<?php 
/** 
 * This sample service contains functions that illustrate typical
 * service operations. This code is for prototyping only. 
 *  
 *  Authenticate users before allowing them to call these methods. 
 */ 

class GCSEFrenchFoundationService { 
  var $username = "gtawebbc_georgew"; 
  var $password = "staiwsv"; 
  var $server = "localhost";
  var $port = "2082";
  var $databasename = "gtawebbc_test"; 
  var $tablename = "FRENCH_FOUNDATION"; 
  
  var $connection; 
  public function __construct() { 
    $this->connection = mysqli_connect( 
                       $this->server,  
                       $this->username,  
                       $this->password, 
                       $this->databasename, 
                       $this->port 
                       ); 
    
    $this->throwExceptionOnError($this->connection); 
  } 

  public function getQuestionsFull() {
     $stmt = mysqli_prepare($this->connection,
          "SELECT
              FRENCH_FOUNDATION.QUESTION,
              FRENCH_FOUNDATION.ANSWER,
              FRENCH_FOUNDATION.WRONG_ANS_1,
              FRENCH_FOUNDATION.WRONG_ANS_2,
              FRENCH_FOUNDATION.WRONG_ANS_3,
              FRENCH_FOUNDATION.EXPLANATION
           FROM FRENCH_FOUNDATION");     
         
      $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->QUESTION, $row->ANSWER,
                    $row->WRONG_ANS_1, $row->WRONG_ANS_2, $row->WRONG_ANS_3,
                    $row->EXPLANATION, $row->UNIT, $row->SECTION,  
                    $row->SUBTITLE, $row->IMAGE, $row->EXAM_BOARD, 
                    $row->COURSE);

      while (mysqli_stmt_fetch($stmt)) {
          $rows[] = $row;
          $row = new stdClass();
          mysqli_stmt_bind_result($stmt, $row->QUESTION, $row->ANSWER,
                    $row->WRONG_ANS_1, $row->WRONG_ANS_2, $row->WRONG_ANS_3,
                    $row->EXPLANATION, $row->UNIT, $row->SECTION,  
                    $row->SUBTITLE, $row->IMAGE, $row->EXAM_BOARD, 
                    $row->COURSE);
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);

      return $rows;
  }  

  public function getQuestionsMin() {
  mysqli_query($this->connection, "SET NAMES utf8");
     $stmt = mysqli_prepare($this->connection,
          "SELECT
              FRENCH_FOUNDATION.QUESTION,
              FRENCH_FOUNDATION.ANSWER,
              FRENCH_FOUNDATION.WRONG_ANS_1,
              FRENCH_FOUNDATION.WRONG_ANS_2,
              FRENCH_FOUNDATION.WRONG_ANS_3,
              FRENCH_FOUNDATION.EXPLANATION
           FROM FRENCH_FOUNDATION");     
         
      $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->QUESTION, $row->ANSWER,
                    $row->WRONG_ANS_1, $row->WRONG_ANS_2, $row->WRONG_ANS_3,
                    $row->EXPLANATION);

      while (mysqli_stmt_fetch($stmt)) {
          $rows[] = $row;
          $row = new stdClass();
          mysqli_stmt_bind_result($stmt,  $row->QUESTION, $row->ANSWER,
                    $row->WRONG_ANS_1, $row->WRONG_ANS_2, $row->WRONG_ANS_3,
                    $row->EXPLANATION);
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);

      return $rows;
  }  
  
/**  SELECT DISTINCT column_name(s) FROM table_nam*/
  
  public function getQuestionsSections() {
     $stmt = mysqli_prepare($this->connection,
          "SELECT DISTINCT
              FRENCH_FOUNDATION.SECTION FROM FRENCH_FOUNDATION");     
         
      $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row->SECTION);

      while (mysqli_stmt_fetch($stmt)) {
          $rows[] = $row;
          $row = new stdClass();
          mysqli_stmt_bind_result($stmt,  $row->SECTION);
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);

      return $rows;
  }
  
  
  
  
  public function getDetails() {
     mysqli_query($this->connection, "SET NAMES utf8");
     $stmt = mysqli_prepare($this->connection,
          "SELECT DISTINCT
              FRENCH_FOUNDATION.DESCRIPTION
           FROM FRENCH_FOUNDATION");     
         
      $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row-> DESCRIPTION);

      while (mysqli_stmt_fetch($stmt)) {
          $rows[] = $row;
          $row = new stdClass();
          mysqli_stmt_bind_result($stmt,  $row-> DESCRIPTION);
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);

      return $rows;
  }
  
/**  query = $mysqli->query("SELECT `columnX` FROM `table` WHERE `columnY` = $value LIMIT 1");
$row = $query->fetch_assoc();
echo $row['columnX'];*/

public function getDetailsBy($itemID) {
      mysqli_query($this->connection, "SET NAMES utf8");
     $stmt = mysqli_prepare($this->connection, 
     "SELECT DISTINCT FRENCH_FOUNDATION.DESCRIPTION
     FROM FRENCH_FOUNDATION WHERE FRENCH_FOUNDATION.SECTION ='$itemID'");
     $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

      $rows = array();
      mysqli_stmt_bind_result($stmt, $row-> DESCRIPTION);

      while (mysqli_stmt_fetch($stmt)) {
          $rows[] = $row;
          $row = new stdClass();
          mysqli_stmt_bind_result($stmt,  $row-> DESCRIPTION);
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);

      return $rows;

      
} 
/**
  
   public function getDetailsBy($itemID) {
      mysqli_query($this->connection, "SET NAMES utf8");
     $stmt = mysqli_prepare($this->connection, 
     "SELECT DISTINCT
     	FRENCH_FOUNDATION.DESCRIPTION
     FROM 'FRENCH_FOUNDATION' WHERE FRENCH_FOUNDATION.SECTION = $itemID");
      $this->throwExceptionOnError();
          
      mysqli_stmt_bind_param($stmt, 'i', $itemID);
      $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

       $row = $query->fetch_assoc();
	return $row['DESCRIPTION'];

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);
      } 
      
      */
  
  public function getLastUpdate() {
	
	$sql = "SHOW TABLE STATUS LIKE 'FRENCH_FOUNDATION'";
	mysqli_close($this->connection);
	$link = mysqli_connect( 
                       $this->server,  
                       $this->username,  
                       $this->password, 
                       $this->databasename, 
                       $this->port 
                       );
                       
     $tableStatus = mysqli_query($link, $sql);
    if (!$tableStatus) {
        $error = 'Error getting update status: ' . mysqli_error($link);
        include 'error.html.php';
        exit();
    }
	
	while ($array = mysqli_fetch_array($tableStatus)) {
		$updatetime = $array['Update_time'];
	}
	return $updatetime;    
  }
  
   public function getQuestionsMinBySection($searchString) {
   mysqli_query($this->connection, "SET NAMES utf8");
     $stmt = mysqli_prepare($this->connection,
           "SELECT
              FRENCH_FOUNDATION.QUESTION,
              FRENCH_FOUNDATION.ANSWER,
              FRENCH_FOUNDATION.WRONG_ANS_1,
              FRENCH_FOUNDATION.WRONG_ANS_2,
              FRENCH_FOUNDATION.WRONG_ANS_3,
              FRENCH_FOUNDATION.EXPLANATION,
              FRENCH_FOUNDATION.SECTION
           FROM FRENCH_FOUNDATION where FRENCH_FOUNDATION.SECTION LIKE ?");
      $this->throwExceptionOnError();
          
      mysqli_stmt_bind_param($stmt, 's', $searchString);
      $this->throwExceptionOnError();

      mysqli_stmt_execute($stmt);
      $this->throwExceptionOnError();

      $rows = array();
      mysqli_stmt_bind_result($stmt,  $row->QUESTION, $row->ANSWER,
                    $row->WRONG_ANS_1, $row->WRONG_ANS_2, $row->WRONG_ANS_3,
                    $row->EXPLANATION, $row->SECTION);

      while (mysqli_stmt_fetch($stmt)) {
          $rows[] = $row;
          $row = new stdClass();
          mysqli_stmt_bind_result($stmt,  $row->QUESTION, $row->ANSWER,
                    $row->WRONG_ANS_1, $row->WRONG_ANS_2, $row->WRONG_ANS_3,
                    $row->EXPLANATION, $row->SECTION);
      }

      mysqli_stmt_free_result($stmt);
      mysqli_close($this->connection);
      
      return $rows;


  } 

/** 
  * Utitity function to throw an exception if an error occurs 
  * while running a mysql command. 
  */ 
  private function throwExceptionOnError($link = null) { 
    if($link == null) { 
      $link = $this->connection; 
    } 
    if(mysqli_error($link)) { 
      $msg = mysqli_errno($link) . ": " . mysqli_error($link); 
      throw new Exception('MySQL Error - '. $msg); 
    }         
  } 
 
} 
?>