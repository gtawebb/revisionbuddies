﻿/**
 * com.sevenson.math.display.mathml.MN
 * 
 * 
 * @author Andrew Sevenson
 * @version 1.0
*/

package com.sevenson.math.display.mathml
{
	import flash.display.Sprite;
	import com.sevenson.math.display.mathml.*;
	import flash.text.TextField;

import model.Constants;

/**
	 * The MN class
	 */
	public class BR extends MathMLBaseSprite
	{
		
		// vars
		private var _alignwidth:Number;
		
		
		/**
		 * Creates a new instance of the MN class
		 */
		public function BR ($node:XML)
		{
			//
			super($node);
			//
			buildContent($node.child('*'));
		}
		
		
		// STATIC PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// STATIC PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------

		
		
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * This will build this item based on the $node passed in
		 * @param	$node
		 */
        private function buildContent($node:XMLList):void {
            //


            // create a text field to hold everything
            var tf:TextField = MathML.generateTextField();
            //
            var txt:String = MathML.parseString($node);

            trace("THE BR STRING = "+txt)

			tf.htmlText = "hello <br> ted";

            //
                      //
            addChild(tf);
        }
		
		
		// PROTECTED FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// EVENT HANDLERS
		// ------------------------------------------------------------------------------------------
		
		
		// GETTERS & SETTERS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * Returns the width that should be used to align this item
		 */

		
	}
}