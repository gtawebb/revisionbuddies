﻿/**
 * com.sevenson.math.display.mathml.MN
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import flash.display.Sprite;
import com.sevenson.math.display.mathml.*;

import flash.text.AntiAliasType;
import flash.text.TextField;

import model.Constants;


/**
 * The MN class
 */
public class I extends MathMLBaseSprite
{
    static public const italicAdjust=5


    // vars
    private var _alignwidth:Number;
    private var moveLeft:Boolean;


    /**
     * Creates a new instance of the MN class
     */
    public function I ($node:XML)
    {
        //
        super($node);
        //
        buildContent($node.child('*'));
    }


    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------



    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * This will build this item based on the $node passed in
     * @param	$node
     */
    private function buildContent($node:XMLList):void {
        //


        // create a text field to hold everything
        var tf:TextField = MathML.generateTextField();
        //
        var txt:String = MathML.parseString($node);

        trace("THE STRING = "+txt);
        var txtTrail:String="";
        var txtPre:String="";

        if(txt.indexOf("@")!=-1)
        {
            moveLeft=true;
            txt=txt.substr(1,txt.length-1)
        }

        var pattern:RegExp = /[a-z]/ig;

        var firstLetterPosition:Number=0;
        var temp:int=txt.search(pattern);
        if(temp>0)
        {
            firstLetterPosition=temp;
        }
        var italicsCharslength:int=txt.match(pattern).length;



        trace("search result ="+firstLetterPosition);
        trace("search result italicsChars="+italicsCharslength);




        txtTrail=txt.substr(firstLetterPosition+italicsCharslength,txt.length);
        txt=txt.substr(0,firstLetterPosition+italicsCharslength);

//                if(txt.charAt(0)=="(")
//                {
//                    txtPre=txt.charAt(0);
//                    txt=txt.substr(1,txt.length-1);
//                }
        if(firstLetterPosition>0)
        {
            txtPre=txt.substr(0,firstLetterPosition);
            txt=txt.substr(firstLetterPosition,txt.length-1);


        }


        trace("THE STRING123 = "+txt)

        tf.htmlText = txtPre+"<i><b>" + txt + "</b></i>"+txtTrail;

//            if(txt.charAt(txt.length-1)=="l"||txt.charAt(txt.length-1)=="f"||txt.charAt(txt.length-1)=="t"||txt.charAt(txt.length-1)=="r")
//            {
        var savedWidth = tf.width + 5;
        var savedHeight = tf.height + 2;

// removing autoSize, wich is the origin of the problem i think
        tf.autoSize = "none";

// now manually autoSizing the textField with saved values
        tf.width = savedWidth;
        tf.height = savedHeight;
//            }


        //
        _alignwidth = tf.width// -4;
        if(Constants.platform()==Constants.PLATFORM_GOOGLE)
        {

           // tf.y=-1;
        }
        else
        {
            tf.y=+1;
        }

        //
        addChild(tf);
        if(moveLeft)
        {
            tf.x=-3;
            _alignwidth = tf.width-3;
        }
    }


    // PROTECTED FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------

    /**
     * Returns the width that should be used to align this item
     */
    override public function get alignWidth():Number {
        return _alignwidth- italicAdjust
    }

    override public function get xalignWithSubSup():Number {
        return - italicAdjust
    }

}
}