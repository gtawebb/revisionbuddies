﻿/**
 * com.sevenson.math.display.mathml.MN
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import flash.display.CapsStyle;
import flash.display.LineScaleMode;
import flash.display.Shape;
import flash.display.Sprite;
import com.sevenson.math.display.mathml.*;
import flash.text.TextField;

/**
 * The MN class
 */
public class MArrowRight extends MathMLBaseSprite
{

    // vars
    private var _alignwidth:Number;


    /**
     * Creates a new instance of the MN class
     */
    public function MArrowRight ($node:XML)
    {
        //
        super($node);
        //
        buildContent($node.child('*'));
    }


    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------



    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * This will build this item based on the $node passed in
     * @param	$node
     */
    private function buildContent($node:XMLList):void {
        //
        if(String(tf)==""){ return; }
        // create a text field to hold everything
        var tf:TextField = MathML.generateTextField();

        //
        var txt:String = MathML.parseString($node);
        //
        tf.htmlText = txt;

        //
        _alignwidth = tf.width -4;

        //
        addChild(tf);

        var wholeArrow:Sprite=new Sprite();


        var angleR:int=0;
        var shArrow:Shape = new Shape();
        shArrow.graphics.lineStyle();
        shArrow.graphics.beginFill(0x000000);
        shArrow.graphics.moveTo(angleR,1);
        shArrow.graphics.lineTo(angleR-4,8);
        shArrow.graphics.lineTo(angleR+4,8);
        shArrow.graphics.lineTo(angleR,1);
        shArrow.graphics.endFill();
        shArrow.rotationZ=90;
        shArrow.scaleX=shArrow.scaleY=.5;
        wholeArrow.addChild(shArrow);
        shArrow.x=tf.width-shArrow.width;
        var line:Sprite = new Sprite();
        // line.y=+0.5;
        // line.x=tf.height/4
        line.graphics.lineStyle(2,  int(MathML.currentTextFormat.color), 1, false, LineScaleMode.NONE, CapsStyle.NONE);
        line.graphics.lineTo(shArrow.x-shArrow.width/2,0);
        wholeArrow.addChild(line);
        addChild(wholeArrow);
        wholeArrow.x=tf.height/8;
        wholeArrow.y=3;

        //tf.y+=3


        addChild(wholeArrow);
        addChild(tf);





    }


    // PROTECTED FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------

    /**
     * Returns the width that should be used to align this item
     */
    override public function get alignWidth():Number {
        return _alignwidth+2;
    }


}
}