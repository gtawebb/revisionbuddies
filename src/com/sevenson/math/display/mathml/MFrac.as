﻿/**
 * com.sevenson.math.display.mathml.MFrac
 * 
 * 
 * @author Andrew Sevenson
 * @version 1.0
*/

package com.sevenson.math.display.mathml
{
	import flash.display.Sprite;
	import com.sevenson.math.display.mathml.*;
	import flash.geom.Rectangle;
	import flash.display.LineScaleMode;
	import flash.display.CapsStyle;
import flash.text.TextField;

/**
	 * The MFrac class
	 */
	public class MFrac extends MathMLBaseSprite
	{
		
		// vars
		protected var _linethickness:Number = 1;
        protected var _gutteroffsetexternal:Number = 3;

    private var theOverallWidth:int;
    private var numerator:MathMLBaseSprite
    private var denominator:MathMLBaseSprite

		/**
		 * Creates a new instance of the MFrac class
		 */
		public function MFrac ($node:XML)
		{
			//
			super($node);
			//
			buildContent($node.child('*'));
		}
		
		
		// STATIC PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// STATIC PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------

		
		
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * This will build this item based on the $node passed in
		 * @param	$node
		 */
        protected function getScale():Number {
            return 1;
        }


		protected function buildContent($node:XMLList):void {
			// check that there is enough nodes
			var len:int = $node.length();
            if(len!=2&&len!=3) {
                throw new Error("MFRAC was given incorrect number of properties:\n" + $node);
                return;
            }
			
			//
			
			// parse the two halves...
			 numerator = MathML.parseMathMLNode($node[0]);
			addChild(numerator);
			
			 denominator = MathML.parseMathMLNode($node[1]);
			addChild(denominator);

            if(getScale()<1)
            {
                //TextField(denominator.getChildAt(0)).htmlText="<b>"+TextField(denominator.getChildAt(0)).htmlText+"<b>"
                //TextField(numerator.getChildAt(0)).htmlText="<b>"+TextField(numerator.getChildAt(0)).htmlText+"<b>"

                numerator.scaleX=numerator.scaleY=getScale();
                denominator.scaleX=denominator.scaleY=getScale();

            }


			// draw in the line
			var tempwidths:Array = [numerator.width+numerator.xalignWithSubSup, denominator.width+denominator.xalignWithSubSup]
			tempwidths.sort(Array.NUMERIC | Array.DESCENDING);
			var line:Sprite = new Sprite();
			if(_linethickness>0) {
				//trace(">>> " + _linethickness);
				line.graphics.lineStyle(_linethickness,  int(MathML.currentTextFormat.color), 1, false, LineScaleMode.NONE, CapsStyle.NONE);
				line.graphics.lineTo(tempwidths[0],0);
				addChild(line);
			}
			// position everything
			//var nrect:Rectangle = numerator.getRect(numerator);
			//var drect:Rectangle = denominator.getRect(denominator);
			
			// vertically
            numerator.y=-3;

            line.y = (numerator.y + numerator.height + _linethickness/2)// nrect.top/4;
            denominator.y = (line.y + _linethickness/2)//-denominator.y// - drect.top/8;


			// horizontally
			if(numerator.width+numerator.xalignWithSubSup>denominator.width+denominator.xalignWithSubSup) {
				denominator.x = ((numerator.width+numerator.xalignWithSubSup)/2) - ((denominator.width+denominator.xalignWithSubSup)/2);
                theOverallWidth=numerator.width+numerator.xalignWithSubSup;
			} else {
				numerator.x =  ((denominator.width+denominator.xalignWithSubSup)/2) - ((numerator.width+numerator.xalignWithSubSup)/2);
                theOverallWidth=denominator.width+denominator.xalignWithSubSup;
			}
			
			// apply the internal offsets
			denominator.x += _gutteroffsetexternal;
			numerator.x += _gutteroffsetexternal;
			line.x += _gutteroffsetexternal;



			
			// apply the offsets, etc...
			numerator.y += (numerator as MathMLBaseSprite).yoffset+2;
			numerator.x += (numerator as MathMLBaseSprite).xoffset;

			denominator.y += (denominator as MathMLBaseSprite).yoffset-1;
			denominator.x += (denominator as MathMLBaseSprite).xoffset;			

			//line.y += (denominator as MathMLBaseSprite).yoffset;
			line.x += (denominator as MathMLBaseSprite).xoffset;

            if(len==3)
            {
                var trailing:Sprite = MathML.parseMathMLNode($node[2]);
                addChild(trailing);
                trailing.y = trailing.height/2+_linethickness-2

                trailing.x=line.x+theOverallWidth;
                trace("trailing has been added")
                // $temp = base.width + " " + sub.width + " " + sup.width+" "+trailing.width;
            }



			
		}		
		
		
		// PROTECTED FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * Parses the attributes from the passed in xml
		 * @param	$xml
		 */
		override protected function parseAttributes($xml:XML):void {
			//
			super.parseAttributes($xml)
			//
			if($xml.@linethickness!=undefined) {
				_linethickness = Number($xml.@linethickness);
				if(_linethickness<0) { _linethickness =0; }
			}
		}			
		
		// EVENT HANDLERS
		// ------------------------------------------------------------------------------------------
		
		
		// GETTERS & SETTERS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * Returns the width that should be used to align this item
		 */
		override public function get alignWidth():Number
        {
            trace("GET ALIGN WIDTH CALLED,= "+width + (_gutteroffsetexternal));
			return width + (_gutteroffsetexternal)//+denominator.xalignWithSubSup+numerator.xalignWithSubSup;
		}
		
	}
}