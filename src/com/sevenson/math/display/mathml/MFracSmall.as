﻿/**
 * com.sevenson.math.display.mathml.MFrac
 * 
 * 
 * @author Andrew Sevenson
 * @version 1.0
*/

package com.sevenson.math.display.mathml
{
	import flash.display.Sprite;
	import com.sevenson.math.display.mathml.*;
	import flash.geom.Rectangle;
	import flash.display.LineScaleMode;
	import flash.display.CapsStyle;
import flash.text.TextField;

/**
	 * The MFrac class
	 */
	public class MFracSmall extends MFrac
	{
		
		// vars

		/**
		 * Creates a new instance of the MFrac class
		 */
		public function MFracSmall ($node:XML)
		{
            //

            super($node);
            //
			//super.buildContent($node.child('*'));
		}


		
		
		// STATIC PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// STATIC PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------

		
		
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * This will build this item based on the $node passed in
		 * @param	$node
		 */

		
		
		// PROTECTED FUNCTIONS
		// ------------------------------------------------------------------------------------------

        override protected function getScale():Number {
            return 0.6;
        }



		/**
		 * Parses the attributes from the passed in xml
		 * @param	$xml
		 */

		
		// EVENT HANDLERS
		// ------------------------------------------------------------------------------------------
		
		
		// GETTERS & SETTERS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * Returns the width that should be used to align this item
		 */

		
	}
}