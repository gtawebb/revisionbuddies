﻿/**
 * com.sevenson.math.display.mathml.MN
 * 
 * 
 * @author Andrew Sevenson
 * @version 1.0
*/

package com.sevenson.math.display.mathml
{
import flash.display.Shape;
import flash.display.Sprite;
	import com.sevenson.math.display.mathml.*;
	import flash.text.TextField;
	
	/**
	 * The MN class
	 */
	public class MR extends MathMLBaseSprite
	{

        protected var _alignheight:Number;
        protected var _trailing:Sprite;



        /**
         * Creates a new instance of the MUnderOver class
         */
        public function MR ($node:XML)
        {
            //
            super($node);
            //
            buildContent($node.child('*'));
        }


        // STATIC PUBLIC FUNCTIONS
        // ------------------------------------------------------------------------------------------


        // STATIC PRIVATE FUNCTIONS
        // ------------------------------------------------------------------------------------------


        // PUBLIC FUNCTIONS
        // ------------------------------------------------------------------------------------------



        // PRIVATE FUNCTIONS
        // ------------------------------------------------------------------------------------------


        // PROTECTED FUNCTIONS
        // ------------------------------------------------------------------------------------------

        /**
         * This will build this item based on the $node passed in
         * @param	$node
         */
        protected function buildContent($node:XMLList):void {
            // check that there is enough nodes
            var len:int = $node.length();
//            if(len!=2) {
//                throw new Error("RECURRING was given incorrect number of properties:\n" + $node);
//                return;
//            }
            //
            if(len==3)
            {

                _trailing = MathML.parseMathMLNode($node[2]);
                trace("trailing sprite ="+_trailing )

            }

            renderRecurringLayout($node[0], $node[1]);
            trace("len ="+len )

        }

        //
        override protected function parseAttributes($xml:XML):void {
            super.parseAttributes($xml)
            //


        }


        //
        // this will render out the given text
        protected function renderRecurringLayout(basenode:XML, overnode:XML):void {
            // parse the base
            var base:Sprite = MathML.parseMathMLNode(basenode);

            var theTextToCopy:String=TextField(base.getChildAt(0)).text;
            var textlength:int=theTextToCopy.length;
            var baseText:TextField = MathML.generateTextField();

            if(textlength>1) {



                var baseTextEnd:TextField = MathML.generateTextField();
                baseText.text = theTextToCopy.substr(0, theTextToCopy.length - 1);
                baseTextEnd.text = theTextToCopy.substr(theTextToCopy.length - 1, theTextToCopy.length);

                trace("baseText= " + baseText.text)
                trace("baseText= " + baseTextEnd.text)

                var newbase:Sprite = new Sprite();
                newbase.addChild(baseText);
                addChild(newbase);

                var newbaseEnd:Sprite = new Sprite();
                newbaseEnd.addChild(baseTextEnd);
                addChild(newbaseEnd);
                //
                //newbase.x = -1;
                newbaseEnd.x = newbase.x + newbase.width-3// - newbaseEnd.width / 4;
                _alignheight = newbase.height;
                // this.height+this.height+4


                // parse the over
                var over:Shape =  createPoint(newbaseEnd.width/3,newbaseEnd.height/5,.7) ;
                addChild(over);


                //
                over.x = newbaseEnd.x + newbaseEnd.width / 4;
               // over.height=over.height/2;


                over.y = -over.height / 1.55;//_213
                trace("_trailing="+_trailing);
                if(_trailing!=null)
                {

                    addChild(_trailing)
                    _trailing.x=newbaseEnd.x+newbaseEnd.width-4;

                }

            }
            else
            {

                baseText.text = theTextToCopy;




                var newbase:Sprite = new Sprite();
                newbase.addChild(baseText);
                addChild(newbase);


               // newbase.x = -2;

                _alignheight = newbase.height;
                // this.height+this.height+4


                // parse the over
                var over:Shape =  createPoint(baseText.width/3,baseText.height/5,.7) ;


                addChild(over);

              //  over.height=over.height/2;



                //
                over.x = newbase.x + newbase.width / 4;

                over.y = -over.height / 1.55;//_213

                if(_trailing!=null)
                {

                    addChild(_trailing)
                    _trailing.x=newbase.x+newbase.width-4;
                }
            }


        }

        private function createPoint(x:Number, y:Number,radio:Number):Shape
        {
            var s:Shape = new Shape();
            s.graphics.beginFill(int(MathML._format.color), 1);
            s.graphics.drawCircle(x,y, radio);
            s.graphics.endFill();

            return s;
        }


        // EVENT HANDLERS
        // ------------------------------------------------------------------------------------------


        // GETTERS & SETTERS
        // ------------------------------------------------------------------------------------------

        /**
         * Returns the height that should be used to align this item
         */
        override public function get alignHeight():Number {
            return _alignheight;

        }

        override public function get alignWidth():Number {
            return width-4;
        }

    }
}