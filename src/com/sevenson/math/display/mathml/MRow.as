﻿﻿﻿/**
 * com.sevenson.math.display.mathml.MRow
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import components.MathItem;
import components.MathItem;

import flash.display.Sprite;
import com.sevenson.math.display.mathml.*;

import flash.text.TextField;

/**
 * The MRow class
 */
public class MRow extends MathMLBaseSprite
{


    /**
     * Creates a new instance of the MRow class
     */
    public function MRow ($node:XML)
    {
        //
        super($node);
        //
        buildContent($node.child('*'));
    }


    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * This will build this item based on the $node passed in
     * @param	$node
     */
    private function buildContent(node_xml:XMLList):void {
        // set up an array to hold each of the lements created
        var item_array:Array = new Array();
        // loop through all of the children, parsing each node and generating clips
        var len:int = node_xml.length();
        var theY:int=0;
        var textToDisplay:String;
        var textLeftOver:String;
        var maxWidth:int= 300;
        var theFullString:String;
        var theCutOffChar:String;
        var theCutOffCharIndex:int;
        var newCount:int;
        var textFieldMeassurer:TextField=MathML.generateTextField();
        var s:Sprite;
        var $n:String;
        var theNewXLoc:int;
        for(var i:int=0; i<len; i++) {
            // parse out each item
            $n = String(node_xml[i].name().localName).toLowerCase();
            if($n=="mtext")
            {
                //do special text breakup
                var s:Sprite=new MI(node_xml[i]);
                trace(s.getChildAt(0));
                var theTextField:TextField = TextField(s.getChildAt(0));
                trace("theTextField.text="+theTextField.text);





                    do
                    {

                        trace("theNewXLoc" + theNewXLoc)
                        theFullString = theTextField.text;

                        theCutOffCharIndex = theTextField.getCharIndexAtPoint(maxWidth, 5);


                        newCount = 0;
                        do
                        {
                            theCutOffChar = theTextField.text.charAt(theCutOffCharIndex - newCount);
                            textFieldMeassurer.text = textFieldMeassurer.text + theCutOffChar;
                            newCount++
                            trace("theCutOfChar=" + theCutOffChar);
                            trace("newCount=" + newCount);
                            trace("theFullString=" + String(theFullString.length));


                        }
                        while (theCutOffChar != " ");


                        var reduceString:String = theFullString.substr(0, theCutOffCharIndex - newCount);
                        theCutOffCharIndex = reduceString.length + 1;

                        // var theCutOfChar:String=theTextField.text.charAt(theCutOfCharIndex);


                        if (theTextField.text.charAt(0) == " ") {
                            textToDisplay = theTextField.text.substr(1, theCutOffCharIndex);

                        }
                        else {

                            textToDisplay = theTextField.text.substr(0, theCutOffCharIndex);
                        }

                        // var textNotToDisplay:String=//
                        theTextField = MathML.generateTextField();
                        trace("theFullString=" + theFullString)

                        theTextField.text = theFullString.substr(theCutOffCharIndex, theFullString.length - 1);
                        //textLeftOver = theTextField.text.substr(theTextField.getCharIndexAtPoint(maxWidth,5),theTextField.text.length-1);

                        var textToAdd:TextField = MathML.generateTextField();
                        textToAdd.text = textToDisplay;
                        var spriteToAdd:MathItem = new MathItem();
                        spriteToAdd.addChild(textToAdd);


                        item_array.push(spriteToAdd);
                        // addChild(spriteToAdd);
                        s = spriteToAdd;
                        spriteToAdd.widthOfDeletedText = textFieldMeassurer.width;
                        trace("item_array.length=" + item_array.length)

                    }
                    while (theTextField.width > maxWidth);

                var newText:String=theTextField.text.substr(1,theTextField.text.length-1);
                theTextField.text=newText;
                var spriteToAdd:MathItem=new MathItem();
                spriteToAdd.addChild(theTextField);


                item_array.push(spriteToAdd);
                // addChild(spriteToAdd);
                s=spriteToAdd;
                spriteToAdd.widthOfDeletedText=textFieldMeassurer.width;
                }




            else
            {

            var h:MathItem=new MathItem();
            var g:Sprite = MathML.parseMathMLNode(node_xml[i]);
            h.widthOfDeletedText=0;
            h.addChild(g)
            item_array.push(h);


            // }



//                addChild(s);
//                if (i > 0) {
//                    s.x = item_array[i - 1].x + item_array[i - 1].width;
//                    s.y =theY
//                    if(s.x+s.width>maxWidth)
//                    {
//
//                        s.x=0;
//                        s.y=theY=item_array[i - 1].y+item_array[i - 1].height+10;
//                    }
//                }
//                if(s is MathMLBaseSprite) {
//                    s.x += MathMLBaseSprite(s).xoffset;
//                }







            // position next to the last



        }
        //
        var totalheight:Number = height;

        // position vertically?
//			for(i=0; i<item_array.length; i++) {
//				//trace("@ " + item_array[i].yoffset);
//				item_array[i].y = (totalheight*0.5) - (item_array[i].alignHeight*0.5) + item_array[i].yoffset;
//			}



        for(var j:int=0; j<item_array.length; j++) {
            s=item_array[j]
            addChild(s)
            if (j > 0) {
                s.x = item_array[j - 1].x + item_array[j - 1].width;

                // if (s.x > maxWidth-textFieldMeassurer.width) {
                if (s.x > maxWidth-MathItem(s).widthOfDeletedText) {

                    s.x = 0;
                    trace("j="+j);
                    trace("greater than scenerario");


//                                    //theY = item_array[j - 1].y + item_array[j - 1].height + 20;
//                                    theY = item_array[j].y + item_array[j].height + 20;
//                                    s.y = theY;
//                                    //theY=theY+item_array[j].height + 20;
                    theY = j*20;

                }

                s.y = theY;

            }

            var totalheight:Number = height;

            // position vertically?
            for(i=0; i<item_array.length; i++) {
                trace("@ " + item_array[i]);
                item_array[i].y = (totalheight*0.5) - (item_array[i].alignHeight*0.5) + item_array[i].yoffset;
            }


            //
            if (s is MathMLBaseSprite) {
                s.x += MathMLBaseSprite(s).xoffset;
            }


        }




    }



    // PROTECTED FUNCTIONS
    // ------------------------------------------------------------------------------------------



    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------


}
}