﻿/**
 * com.sevenson.math.display.mathml.MRow
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml {
import flash.display.Sprite;

import com.sevenson.math.display.mathml.*;

/**
 * The MRow class
 */
public class MRow_bak2 extends MathMLBaseSprite {
    private var ycount:int = 0;
    private var mywidth:int;


    /**
     * Creates a new instance of the MRow class
     */
    public function MRow_bak2($node:XML) {
        //
        super($node);
        //
        buildContent($node.child('*'));
    }


    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * This will build this item based on the $node passed in
     * @param    $node
     */
    private function buildContent(node_xml:XMLList):void {
        // set up an array to hold each of the lements created
        var item_array:Array = new Array();
        // loop through all of the children, parsing each node and generating clips
        var len:int = node_xml.length();


        for (var i:int = 0; i < len; i++) {
            // parse out each item
            trace("the node [i] = " + node_xml[i]);

            var $n = String(node_xml[i].name().localName).toLowerCase();


            var s:MathMLBaseSprite = MathML.parseMathMLNode(node_xml[i]);
            mywidth=mywidth +s.alignWidth// + s.xalignWithSubSup;

            item_array.push(s);
            addChild(s);

            if (i > 0) {
                s.x = item_array[i - 1].x + item_array[i - 1].alignWidth;
                if (s.x > 400) {
                    ycount++
                    s.x = 0;
                    // s.y=30*ycount;

                }
            }

            if (s is MathMLBaseSprite) {
                // trace("the xoffset= "+MathMLBaseSprite(s).xoffset);
                s.x += MathMLBaseSprite(s).xoffset;
            }
            // position next to the last
            // trace("adding the row contents, x=  "+s.x);


        }
        //
        var totalheight:Number = height;

        // position vertically?
        for (i = 0; i < item_array.length; i++) {
            // trace("item_array[i].yoffset: " + item_array[i].yoffset);

            // trace("item_array[i].alignHeight: " + item_array[i].alignHeight);
            //  trace("totalheight: " + totalheight);


            item_array[i].y = (totalheight * 0.5) - (item_array[i].alignHeight * 0.5) + item_array[i].yoffset;
        }

    }

    // PROTECTED FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------


    override public function get alignWidth():Number {
        return mywidth;

    }
}
}
