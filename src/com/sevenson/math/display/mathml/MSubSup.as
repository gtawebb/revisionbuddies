﻿/**
 * com.sevenson.math.display.mathml.MSubSup
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import com.sevenson.math.display.mathml.MathMLBaseSprite;

import flash.display.Sprite;
import com.sevenson.math.display.mathml.*;

import flash.text.TextField;

/**
 * The MSubSup class
 */
public class MSubSup extends MathMLBaseSprite
{

    // vars
    private var _alignheight:Number;
    protected var _alignwidth:Number;
    private var _gutteroffsetinternal:Number = -1;

    private var $temp:String;

    protected var baseIsItalic:Boolean=false;

    /**
     * Creates a new instance of the MSubSup class
     */
    public function MSubSup ($node:XML)
    {
        //
        super($node);
        //
        buildContent($node.child('*'));
    }


    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PROTECTED FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // this should build the required content
    protected function buildContent(node_xml:XMLList):void {
        // check that there is enough nodes
        var len:int = node_xml.length();

        trace("node_xml = "+node_xml);
        if(len!=3&&len!=4) {
            throw new Error("MSubSup was given incorrect number of properties:\n" + node_xml);
            return;
        }


        if(len==3) {
            renderSubSupLayout(node_xml[0], node_xml[1], node_xml[2]);

        }
        if(len==4) {
            renderSubSupLayout(node_xml[0], node_xml[1], node_xml[2],node_xml[3]);

        }

    }


    // this will render out the given text
    protected function renderSubSupLayout(basenode:XML, subnode:XML, supnode:XML, trailingnode:XML=null):void {
        // parse the base
        var base:MathMLBaseSprite = MathML.parseMathMLNode(basenode);
        addChild(base);

        trace("base node = "+basenode);


        var thelength:int=basenode.toString().length;



        _alignheight = base.height;

        // parse the sub
        var sub:Sprite = MathML.parseMathMLNode(subnode);
        addChild(sub);
        sub.scaleX = sub.scaleY = MathML.subSupScale;

        // parse the sup
        var sup:Sprite = MathML.parseMathMLNode(supnode);

        addChild(sup);
        sup.scaleX = sup.scaleY = MathML.subSupScale;

        // position it
        //base.y = sup.height * 0.4;



        sup.x = (base.x + base.alignWidth) + _gutteroffsetinternal;
        sup.y =base.y-base.yalignWithSubSup;
        trace("base hight = "+base.height)
        trace("sup hight = "+sup.height)
        sub.x = (base.x + base.alignWidth) + _gutteroffsetinternal;
        sub.y = (base.y + base.height) - (sub.height * 1);

        trace("trailingnode = "+trailingnode)

        if(sub.width>sup.width)
        {
            _alignwidth=MathMLBaseSprite(sub).alignWidth+sub.x;

        }
        else
        {
            _alignwidth=MathMLBaseSprite(sup).alignWidth+sup.x;

        }

        if(basenode.toString().search("@")!=-1)
        {
            sub.x=sub.x-1;
            sup.x=sup.x-1;
        }
        if(trailingnode!=null)
        {
            var trailing:MathMLBaseSprite = MathML.parseMathMLNode(trailingnode);
            addChild(trailing);
            trailing.y=base.y;
            trailing.x=sup.x+MathMLBaseSprite(sup).alignWidth+ _gutteroffsetinternal*2;
            trace("trailing has been added")
            _alignwidth=trailing.x+trailing.alignWidth;

        }
        // trace("the base node = "+basenode.toString())
        // trace("the base node = "+basenode.toString().search("@"))




    }

    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------

    /**
     * Returns the height that should be used to align this item
     */
    override public function get alignHeight():Number {
        return _alignheight;
    }
    override public function get alignWidth():Number {
        return _alignwidth;
    }


}
}