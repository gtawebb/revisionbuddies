﻿/**
 * com.sevenson.math.display.mathml.MSup
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import com.sevenson.math.display.mathml.MathMLBaseSprite;

import flash.display.Sprite;
import com.sevenson.math.display.mathml.*;

/**
 * The MSup class
 */
public class MSupLeft extends MSubSup
{

    // vars

    /**
     * Creates a new instance of the MSup class
     */

    private var _alignheight:Number;
    private var _gutteroffsetinternal:Number = -2;
    private var $temp:String;



    public function MSupLeft ($node:XML)
    {
        //
        super($node);
    }


    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PROTECTED FUNCTIONS
    // ------------------------------------------------------------------------------------------

    // this should build the required content
    override protected function buildContent(node_xml:XMLList):void {
        // check that there is enough nodes
        var len:int = node_xml.length();
        if(len!=2&&len!=3) {
            throw new Error("MSup was given incorrect number of properties:\n" + node_xml);
            return;
        }
        if(node_xml.toString().substring(0,3)=="<i>")
        {
            baseIsItalic=true;
        }

        //
        var tmp:XML = <mn></mn>
        //
        renderSubSupLayout(node_xml[0], tmp, node_xml[1]);

    }


    override protected function renderSubSupLayout(basenode:XML, subnode:XML, supnode:XML, trailingnode:XML=null):void {
        // parse the base
        var base:Sprite = MathML.parseMathMLNode(basenode);
        addChild(base);
        //
        _alignheight = base.height;


        var sup:Sprite = MathML.parseMathMLNode(supnode);

        addChild(sup);
        sup.scaleX = sup.scaleY = MathML.subSupScale;

        // position it
        base.x = MathMLBaseSprite(sup).alignWidth + _gutteroffsetinternal;
        sup.y = -(sup.height * 0.0);




        _alignwidth=MathMLBaseSprite(base).alignWidth+base.x;

        if(trailingnode!=null)
        {
            var trailing:MathMLBaseSprite = MathML.parseMathMLNode(trailingnode);
            addChild(trailing);
            trailing.y=base.y;
            trailing.x=sup.x+MathMLBaseSprite(sup).alignWidth+ _gutteroffsetinternal*2;
            trace("trailing has been added")
            _alignwidth=trailing.x+trailing.alignWidth;

        }

    }

    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------



}
}