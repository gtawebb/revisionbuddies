﻿/**
 * com.sevenson.math.display.mathml.MSup
 * 
 * 
 * @author Andrew Sevenson
 * @version 1.0
*/

package com.sevenson.math.display.mathml
{
	import flash.display.Sprite;
	import com.sevenson.math.display.mathml.*;
	
	/**
	 * The MSup class
	 */
	public class MSupLeftRight extends MSubSup
	{
		
		// vars
		
		/**
		 * Creates a new instance of the MSup class
		 */

        private var _alignheight:Number;
        private var _gutteroffsetinternal:Number = -2;
        private var $temp:String;



        public function MSupLeftRight ($node:XML)
		{
			//
			super($node);
		}
		
		
		// STATIC PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// STATIC PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PROTECTED FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		// this should build the required content



        override protected function renderSubSupLayout(basenode:XML, leftone:XML, rightone:XML, trailingnode:XML=null):void {
            // parse the base
            var base:Sprite = MathML.parseMathMLNode(basenode);
            addChild(base);
            //
            _alignheight = base.height;

            // parse the sub
            var left:Sprite = MathML.parseMathMLNode(leftone);
            addChild(left);
            left.scaleX = left.scaleY = MathML.subSupScale;

            var right:Sprite = MathML.parseMathMLNode(rightone);

            addChild(right);   addChild(right);
            right.scaleX = right.scaleY = MathML.subSupScale;

            // position it
            base.x = left.width*MathML.subSupScale - _gutteroffsetinternal;
            //sup.x = (base.x + base.width) + _gutteroffsetinternal;
            left.y = -(right.height * 0.0);
            right.x = (base.x + base.width) + _gutteroffsetinternal;
            right.y = (base.y + base.height) - (left.height * 1);

            //$temp = base.width + " " + left.width + " " + right.width;

            _alignwidth=MathMLBaseSprite(base).alignWidth+base.x+right.width;

            if(trailingnode!=null)
            {
                var trailing:Sprite = MathML.parseMathMLNode(trailingnode);
                addChild(trailing);
                trailing.y=base.y;
                trailing.x=right.x+right.width;
            }

        }
		
		// EVENT HANDLERS
		// ------------------------------------------------------------------------------------------
		
		
		// GETTERS & SETTERS
		// ------------------------------------------------------------------------------------------
		

		
	}
}