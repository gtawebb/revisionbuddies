﻿/**
 * com.sevenson.math.display.mathml.MFrac
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import flash.display.Shape;
import flash.display.Sprite;
import com.sevenson.math.display.mathml.*;
import flash.geom.Rectangle;
import flash.display.LineScaleMode;
import flash.display.CapsStyle;
import flash.text.TextField;

/**
 * The MFrac class
 */
public class MVect extends MFrac
{

    // vars

    /**
     * Creates a new instance of the MFrac class
     */
    public function MVect ($node:XML)
    {
        //

        super($node);

        //
        //super.buildContent($node.child('*'));
    }




    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------



    // PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * This will build this item based on the $node passed in
     * @param	$node
     */



        // PROTECTED FUNCTIONS
        // ------------------------------------------------------------------------------------------

    override protected function buildContent($node:XMLList):void {
        // check that there is enough nodes

        _gutteroffsetexternal=_gutteroffsetexternal*getScale()

        _linethickness=0.1;
        var len:int = $node.length();
        if(len!=2&&len!=3) {
            throw new Error("MFRAC was given incorrect number of properties:\n" + $node);
            return;
        }

        //

        // parse the two halves...
        var numerator:Sprite = MathML.parseMathMLNode($node[0]);
        addChild(numerator);

        var denominator:Sprite = MathML.parseMathMLNode($node[1]);
        addChild(denominator);







        //TextField(denominator.getChildAt(0)).htmlText="<b>"+TextField(denominator.getChildAt(0)).htmlText+"<b>"
        //TextField(numerator.getChildAt(0)).htmlText="<b>"+TextField(numerator.getChildAt(0)).htmlText+"<b>"

        numerator.scaleX=numerator.scaleY=getScale();
        denominator.scaleX=denominator.scaleY=getScale();




        // draw in the line
        var tempwidths:Array = [numerator.width, denominator.width]
        tempwidths.sort(Array.NUMERIC | Array.DESCENDING);
        var line:Sprite = new Sprite();
        if(_linethickness>0) {
            //trace(">>> " + _linethickness);
            line.graphics.lineStyle(_linethickness,  int(MathML.currentTextFormat.color), 1, false, LineScaleMode.NONE, CapsStyle.NONE);
            line.graphics.lineTo(tempwidths[0],0);
            addChild(line);
        }
        // position everything

        // vertically
        numerator.y=-3;

        line.y = (numerator.y + numerator.height + _linethickness)// nrect.top/4;
        denominator.y = (line.y + _linethickness)//-denominator.y// - drect.top/8;

        // horizontally
        var theOverallWidth:int;
        if(numerator.width>denominator.width) {
            //denominator.x = (numerator.width/2) - (denominator.width/2);
            theOverallWidth=numerator.width;
        } else {
            // numerator.x = (denominator.width/2) - (numerator.width/2);
            theOverallWidth=denominator.width;
        }

        var leftBracketShape:Shape = new Shape();
        leftBracketShape.graphics.lineStyle(1.3, int(MathML._format.color));

        var bracketHeight=(denominator.height/1.5+numerator.height/1.5)
        var bracketWidth=bracketHeight/2;

        addChild(leftBracketShape);
        leftBracketShape.graphics.moveTo(0, 0);
        leftBracketShape.graphics.curveTo(-bracketWidth/2, bracketHeight/2, 0, bracketHeight);
        leftBracketShape.y=line.y+1-leftBracketShape.height/2;
        leftBracketShape.x=bracketWidth/2;

        // apply the internal offsets
        // denominator.x += _gutteroffsetexternal+leftBracketShape.width/3;
        // numerator.x += _gutteroffsetexternal+leftBracketShape.width/3;
        // line.x += _gutteroffsetexternal+leftBracketShape.width/3;




        // apply the offsets, etc...
        numerator.y += (numerator as MathMLBaseSprite).yoffset+2;
        // numerator.x += (numerator as MathMLBaseSprite).xoffset;

        denominator.y += (denominator as MathMLBaseSprite).yoffset-1;
        // denominator.x += (denominator as MathMLBaseSprite).xoffset;

        //line.y += (denominator as MathMLBaseSprite).yoffset;
        //  line.x += (denominator as MathMLBaseSprite).xoffset;

        if(len==3)
        {
            var trailing:Sprite = MathML.parseMathMLNode($node[2]);
            addChild(trailing);
            trailing.y = trailing.height/2-2;

            trailing.x=line.x+leftBracketShape.width+theOverallWidth;
            trace("trailing has been added")
            // $temp = base.width + " " + sub.width + " " + sup.width+" "+trailing.width;
        }


        var rightBracketShape:Shape = new Shape();
        rightBracketShape.graphics.lineStyle(1.3, int(MathML._format.color));

        bracketHeight=(denominator.height/1.5+numerator.height/1.5)
        bracketWidth=bracketHeight/2;

        addChild(rightBracketShape);
        rightBracketShape.graphics.moveTo(0, 0);
        //var bHeight=

        rightBracketShape.graphics.curveTo(bracketWidth/2, bracketHeight/2, 0, bracketHeight);
        rightBracketShape.x=leftBracketShape.width+line.width
        rightBracketShape.y=leftBracketShape.y;

        denominator.x =leftBracketShape.width+(theOverallWidth/2)-denominator.width/2+_gutteroffsetexternal/2
        numerator.x =leftBracketShape.width+(theOverallWidth/2)-numerator.width/2+_gutteroffsetexternal/2
        //numerator.x += _gutteroffsetexternal+leftBracketShape.width/3;

        removeChild(line);
        line=null;






    }



    /**
     * Parses the attributes from the passed in xml
     * @param	$xml
     */


    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------

    /**
     * Returns the width that should be used to align this item
     */


}
}