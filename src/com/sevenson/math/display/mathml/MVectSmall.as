﻿/**
 * com.sevenson.math.display.mathml.MFrac
 * 
 * 
 * @author Andrew Sevenson
 * @version 1.0
*/

package com.sevenson.math.display.mathml
{
import flash.display.Shape;
import flash.display.Sprite;
	import com.sevenson.math.display.mathml.*;
	import flash.geom.Rectangle;
	import flash.display.LineScaleMode;
	import flash.display.CapsStyle;
import flash.text.TextField;

/**
	 * The MFrac class
	 */
	public class MVectSmall extends MVect
	{
		
		// vars

		/**
		 * Creates a new instance of the MFrac class
		 */
		public function MVectSmall ($node:XML)
		{
            //

            super($node);

            //
			//super.buildContent($node.child('*'));
		}

    override protected function getScale():Number {
        return 0.6;
    }


		
		
		// STATIC PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// STATIC PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		
		// PUBLIC FUNCTIONS
		// ------------------------------------------------------------------------------------------

		
		
		// PRIVATE FUNCTIONS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * This will build this item based on the $node passed in
		 * @param	$node
		 */

		
		
		// PROTECTED FUNCTIONS
		// ------------------------------------------------------------------------------------------





		/**
		 * Parses the attributes from the passed in xml
		 * @param	$xml
		 */

		
		// EVENT HANDLERS
		// ------------------------------------------------------------------------------------------
		
		
		// GETTERS & SETTERS
		// ------------------------------------------------------------------------------------------
		
		/**
		 * Returns the width that should be used to align this item
		 */

		
	}
}