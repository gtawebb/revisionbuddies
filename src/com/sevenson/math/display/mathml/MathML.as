﻿/**
 * com.sevenson.math.display.mathml.MathML
 *
 * This class is the main class for parsing MathML.
 * Pass the required MathML to the static method .parse() and it will return a sprite representations.
 * The parsing and create is done via recursion through the various nodes.
 *
 * Note: 	Not all tags are supported yet - interegate the parseMathMLNode method for a list of supported tags.
 * 		 	The following address was used as a guide for creating these classes
 * 			http://www.dessci.com/en/support/mathtype/tutorials/mathml/presentation.htm
 *
 *
 * @author Andrew Sevenson
 * @version 1.0
 */

package com.sevenson.math.display.mathml
{
import com.sevenson.math.display.mathml.MathMLBaseSprite;



import flash.display.DisplayObject;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFieldAutoSize;
import flash.display.Sprite;

import flash.text.TextFormatAlign;


/**
 * The MathML class provides a set of static properties and methods
 */
public class MathML
{

    static private var _format_default:TextFormat;		// this will hold the default txt (for attributes)
    static private var _embedFonts_default:Boolean;
    static private var _currentX:int=0;
    static private var _currentY:int=0;
    static private var _yCount:int=1;
    static public var _format:TextFormat;				// holds the current format for this parse
    static private var _embedFonts:Boolean;
    static public var theMaxWidth:int = 0;
    static private var _line:Array=[];
    private static var theLatestWidthOfDeletedText:int;
    public static var textSize:int;
    // private static var recurringX:int;
    private static var maxHeight:int;


    /*
     * Static constructor
     */
    {

        trace("RUNNING THE STATIC CONSTRUCTOR")
        resetDefaultTextFormat();
        _embedFonts_default = false;
    }

    /**
     * [Static class] MathML cannot be directly instantiated
     */
    public function MathML ()
    {
        throw new Error("MathML cannot be directly instantiated.");
    }




    // STATIC PUBLIC FUNCTIONS
    // ------------------------------------------------------------------------------------------


    /**
     *
     * @param	$xml			XML - the MathML to parse
     * @param	$format			TextFormat - the text format to apply to the elements created - when null, defaults to the defaultTextFormat
     * @param	$embedFonts		Object - should embeded fonts be used - should be boolean (null results in default embedFonts settings)
     * @return	Sprite
     */
    static public function parse($xml:XML, $format:TextFormat=null, $embedFonts:Object=null):Sprite {

        _currentX=0;
        _currentY=0;
        _yCount=0;
        _line=[];


        // make sure there is a format, text field, etc
        _format = ($format == null) ? _format_default : $format;
        _embedFonts = ($embedFonts==null) ? _embedFonts_default : Boolean(_embedFonts);

        // take the given $xml and srap it in an MROW


        //first strip out into an array of xml chunks based on whether they are <mtext> or not

        trace("the $xml = "+$xml);

        var $s:Sprite = new Sprite();

        var $mobj:MathMLBaseSprite;
        var textFieldArray:Array;

        for(var i:int=0;i<$xml.children().length();i++)
        {
            var _row:String = $xml.children()[i].toXMLString();

            trace("the _row = "+_row);

            //loop through the xml and parse the maths and text objectd

            if(_row.substr(0,7)=="<mtext>")
            {
                trace("this text  = "+XMLList($xml.children()[i]).name().localName);

                //split the long text field into an array of small sized ones that fix together
                textFieldArray=splitTheText($xml.children()[i],_currentX);

                for each (var txt:TextField in textFieldArray) {
                    trace("adding a text field, widthOfDeletedText = "+int(txt.name));

                    trace("THE TEXT = "+txt.text);

                    $s.addChild(txt);
                    trace("THE CURRENT X = "+_currentX);
                    alignTheObject(txt,$s,txt.width);

                }
            }
            else
            {
                var $newmml:XML = new XML('<mrow>'+_row+'</mrow>');
                $mobj = parseMathMLNode($newmml);

                if(_row=="<br/>")
                {
                    _line=[];
                    _currentY=_currentY+maxHeight/1.2;
                    _currentX=0;
                }
                else
                {

                    $s.addChild($mobj);
                    alignTheObject($mobj,$s, MathMLBaseSprite($mobj.getChildAt(0)).alignWidth);
                }


            }


        }

        //next loop through them and get an array of sprite to position



        //


        //trace("adding main mathML, x="+$mobj.x)
        //
        _format = null;
        _embedFonts = _embedFonts_default;
        //
        return $s;
    }

    private static function reduceTheWorkingProgressToTheNearestSpace(stringToBeReduced:String):String
    {
        var cutOffIndex:int=stringToBeReduced.lastIndexOf(" ")+1;
        if(cutOffIndex==-1)
        {
            cutOffIndex=0;
        }
        var reducedString:String= stringToBeReduced.substr(0,cutOffIndex);
        var tempTextField:TextField = new TextField();
        tempTextField.text=reducedString;
        if(tempTextField.length>theMaxWidth)
        {
            throw new Error("the text field is too long!" + reducedString);

        }
        return reducedString;
    }

    private static function splitTheText(node_xml:XML, startingX:int):Array {

        var item_array:Array = new Array();
        // loop through all of the children, parsing each node and generating clips

        var textToDisplay:String;

        var theFullStringBeingReduced:String;
        var theCutOffChar:String;
        var theCutOffCharIndex:int;
        var newCount:int;
        var textFieldMeassurer:TextField = MathML.generateTextField();
        var theNewXLoc:int;
        var theWorkingProgessString:String

        //turn the XML into a long text field

        var s:Sprite = new MI(node_xml[0]);
        trace(s.getChildAt(0));
        var theTextField:TextField = TextField(s.getChildAt(0));
        theTextField.text=theTextField.text;
        trace("theTextField.text=" + theTextField.text);

        //if the length and position of the text field means it will spill over the max width, cut it up into smaller bits
        if(theTextField.width+startingX>theMaxWidth)
        {
            // while theTextField.width > maxWidth cut it up
            do
            {


                trace("maxWidth" + theMaxWidth)
                trace("startingX" + startingX)
                theFullStringBeingReduced = theTextField.text;

                //the char index where the cut needs to be
                theCutOffCharIndex = theTextField.getCharIndexAtPoint(theMaxWidth-startingX-2, 5);
                theCutOffChar = theTextField.text.charAt(theCutOffCharIndex);
                theWorkingProgessString = theFullStringBeingReduced.substr(0, theCutOffCharIndex);

                trace("theCutOffCharIndex=" + theCutOffCharIndex);
                trace("theCutOfChar=" + theCutOffChar);
                trace("theWorkingProgessString=" + theWorkingProgessString);

                startingX=0


                if(theCutOffChar!=" ")
                {
                    var theReturnedWorkingProgressString = reduceTheWorkingProgressToTheNearestSpace(theWorkingProgessString);
                    theWorkingProgessString = theReturnedWorkingProgressString;

                    trace("theReturnedWorkingProgressString = "+theReturnedWorkingProgressString);

                    var textToAdd:TextField = MathML.generateTextField();
                    textToAdd.text = theWorkingProgessString;



                }
                trace("theWorkingProgessString = "+theWorkingProgessString);

                var textToAdd:TextField = MathML.generateTextField();
                textToAdd.text = theWorkingProgessString;

                item_array.push(textToAdd);
                trace("item_array.length=" + item_array.length);
                trace("textToAdd.text no spaces="+textToAdd.text);
                // theTextField.text=theFullStringBeingReduced.substr(theWorkingProgessString.length,theFullStringBeingReduced.length);
                var i:int = 1;
                while (textToAdd.text.charAt(0)==" " )
                {
                    trace("getting rid of starting spaces");
                    var temp:String=textToAdd.text.substr(1,textToAdd.text.length)


                    textToAdd.text=temp;
                    i++;
                }
                theTextField.text=theFullStringBeingReduced.substr(theWorkingProgessString.length,theFullStringBeingReduced.length);




                //startingX=textToAdd.width;
                trace("!!!!!!!!!!!!!    startingX ="+startingX)
                theFullStringBeingReduced=theTextField.text;


            }
            while (theTextField.width > theMaxWidth);
            // theTextField.text=theFullStringBeingReduced;
            var i:int = 1;
            trace("theTextField.text no spaces="+theTextField.text.charCodeAt(0));
            while (theTextField.text.charCodeAt(0)==160||theTextField.text.charCodeAt(0)==32)
            {
                trace("getting rid of starting spaces 2");
                var temp:String=theTextField.text.substr(1,theTextField.text.length)


                theTextField.text=temp;
                i++;
            }
            item_array.push(theTextField);
            // startingX=maxWidth-theTextField.width+startingX;

        }
        else
        {
            item_array.push(theTextField);
        }



        return item_array;



    }



    private static function alignTheObject(disObject:Object, $s:Sprite, alignWidth:Number):void
    {
        // recurringX=0;

        //trace(text = textFie)



        disObject.y=_currentY;
        disObject.x=_currentX;

        trace("!!!  alignWidth="+alignWidth);

        _currentX=disObject.x+alignWidth;




        if(_currentX>theMaxWidth)
        {

            _line=[];
            _currentY=disObject.y+maxHeight/1.3;
            trace("! maxHeight =:"+maxHeight);

            _currentX=0;
            disObject.y=_currentY;
            disObject.x=_currentX;

            _currentX=disObject.x+alignWidth;



        }
        _line.push(disObject);
        midPintLine(_currentY);

    }






    private static function midPintLine(theY):void
    {
        var heightsArray:Array=[];



        trace("midpointing the line, current y = "+_currentY)

        for each (var mySprite:DisplayObject in _line)
        {
            trace("mySprite.height = "+mySprite.height)

            if(mySprite.height<19)
            {



                heightsArray.push(16);
            }
            else
            {

                heightsArray.push(mySprite.height);
            }
            trace("heightsArray="+heightsArray)

        }
        maxHeight=Math.max.apply(null, heightsArray);
        trace("set maxHeight to:"+maxHeight);
        for each (var mySprite:DisplayObject in _line)
        {
            mySprite.y=theY+(maxHeight/2-mySprite.height/2);
        }
    }


    /**
     * Resets the default text format to plain text
     */
    static public function resetDefaultTextFormat():void {
        // set up a default text format
        _format_default = new TextFormat();
        _format_default.font = "Arial";
        _format_default.size = textSize;
        _format_default.color = 0x000000;
    }

    /**
     * Set's the default text format to use when parsing.
     */
    static public function get defaultTextFormat():TextFormat { return _format_default; }
    static public function set defaultTextFormat($format:TextFormat):void {
        _format_default = $format;

    }


    /**
     * Set's the default 'embedFonts' property
     */
    static public function get defaultEmbedFonts():Boolean { return _embedFonts_default; }
    static public function set defaultEmbedFonts($value:Boolean):void {
        _embedFonts_default = $value;

    }



    // STATIC PRIVATE FUNCTIONS
    // ------------------------------------------------------------------------------------------


    // STATIC INTERNAL FUNCTIONS
    // ------------------------------------------------------------------------------------------

    /**
     * This will look at the given xml node and generate the required sprite
     * @param	node_xml
     * @return
     */
    static internal function parseMathMLNode($node_xml:XML):MathMLBaseSprite {
        //
        var $n:String = String($node_xml.name().localName).toLowerCase();
        switch ($n) {
            //-- Token Elements --//
            case "mi":
                return new MI($node_xml);
                break;
            case "mr":
                return new MR($node_xml);
                break;
            case "i":
                return new I($node_xml);
                break;
            case "br":
                return new BR($node_xml);
                break;
            case "mn":
                return new MN($node_xml);
                break;
            case "marrowright":
                return new MArrowRight($node_xml);
                break;
            case "mo":
                return new MO($node_xml);
                break;
            case "mtext":
                return new MText($node_xml);
                break;
            case "mspace":
                return new MSpace($node_xml);
                break;

            //-- General Layout:  --//
            case "mrow":
                return new MRow_bak2($node_xml);
                break;

            case "mfrac":
                return new MFrac($node_xml);
                break;
            case "mfracsmall":
                return new MFracSmall($node_xml);
                break;

            case "mvect":
                return new MVect($node_xml);
                break;

            case "mvectsmall":
                return new MVectSmall($node_xml);
                break;

            case "mroot":
                return new MRoot($node_xml);
                break;

            case "msqrt":
                return new MSqrt($node_xml);
                break;


            //-- Scripts and Limits:  --//
            case "msub":
                return new MSub($node_xml);
                break;

            case "msup":
                return new MSup($node_xml);
                break;

            case "msupleft":
                return new MSupLeft($node_xml);
                break;
            case "msupleftright":
                return new MSupLeftRight($node_xml);
                break;

            case "msubsup":
                return new MSubSup($node_xml);
                break;

            case "munderover":
                return new MUnderOver($node_xml);
                break;

            case "munder":
                return new MUnder($node_xml);
                break;

            case "mover":
                return new MOver($node_xml);
                break;


            case "mfenced":
                return new MFenced($node_xml);
                break;

            default:
                throw new Error('MathML - Unsupported node type: '+ $node_xml.name());
                break;

        }
    }




    /**
     * generates a text field to use inside a node (applyes the given format)
     * @param	$format
     * @return
     */
    static internal function generateTextField():TextField {

        var $tf:TextField = new TextField();
        $tf.autoSize = TextFieldAutoSize.LEFT;
        $tf.multiline = false;
        $tf.wordWrap = false;
        $tf.selectable = false;
        //  $tf.border=true;

//            var inputFormat:TextFormat = new TextFormat();
//            inputFormat.align = TextFormatAlign.LEFT;
//
//            inputFormat.italic = true;
//            inputFormat.bold = true;
//            inputFormat.size = 30;
        $tf.defaultTextFormat = _format;
        $tf.setTextFormat(_format);

        $tf.embedFonts = _embedFonts;

        return $tf;
    }

    //
    static internal function parseString(str:String):String {
        return str;
    }






    // EVENT HANDLERS
    // ------------------------------------------------------------------------------------------


    // GETTERS & SETTERS
    // ------------------------------------------------------------------------------------------

    // used to calculate the size of super/subscript
    static internal function get subSupScale():Number {
        var size:Number;
        if(MathML.textSize<16)
        {
            size=0.7;

        }
        else
        {
            size=0.6;
        }
        return size;



    }

    // returns the current format being used to parse
    static internal function get currentTextFormat():TextFormat { return _format; }




}
}