package components
{
	import flash.events.Event;
	import flash.text.TextField;
	
	import spark.components.Button;
	import spark.components.supportClasses.StyleableTextField;
	
	
	
	public class DeSkinnableButton extends spark.components.Button
	{
		
		//private var _content:*;
		
		public function DeSkinnableButton()
		{
			super();
		
		}
		
		
		
		
		
		override public function initialize():void {
			super.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			super.addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
			
			super.initialize();
		}
		
		private function addedToStageHandler(event:Event):void {
			if (super.skin == null) super.attachSkin();
		}
		
		private function removedFromStageHandler(event:Event):void {
			super.detachSkin();
		}
	}
}