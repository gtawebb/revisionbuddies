package components
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import controller.RevisionBuddiesFatController;
	
	import flashx.textLayout.formats.TextAlign;

import model.Constants;

import skins.assets.BlueButtonDown;
	import skins.assets.BlueButtonUp;
	import skins.assets.Cross;
import skins.assets.GreenButtonDown;
import skins.assets.GreenButtonUp;
	import skins.assets.RedButtonUp;
	import skins.assets.Tick;







	public class RBButton extends Sprite
	{
		public static const FIRED:String="FIRED";
		public static const CLICKED:String="CLICKED";
		public var blueDown:BlueButtonDown=new BlueButtonDown();
		public var blueUp:BlueButtonUp=new BlueButtonUp();
		
		private var butUP:Boolean=true;

		private var label:TextField=new TextField();
		private var labelText:String;
		public var backGround:Sprite=new Sprite();

		private var _correctAns:Boolean;

		private var scaleFactor:int;
		private var _id:String;

		public var centreText:Boolean;

        private var greenUp:GreenButtonUp=new GreenButtonUp();
        private var greenDown:GreenButtonDown=new GreenButtonDown();

		public var _fatcontroller:RevisionBuddiesFatController;
        private var _textSize:int;

		
		public function RBButton(scaleFactor:Number,butWidth:int, centreText:Boolean=false, textSize:int=12)
		{



			this.centreText = centreText;
			this.scaleFactor = scaleFactor;
            _textSize = textSize;

            this.mouseChildren=false;
			
			

			addChild(backGround);
			//this.height=50
			backGround.addChild(blueDown);
			backGround.addChild(blueUp);
			backGround.width=butWidth;
			backGround.height=47*Constants.SCALE_Y;
			blueDown.visible=false;
			blueUp.visible=true;
			butUP=true;
			addEventListener(MouseEvent.MOUSE_UP,upState,false,0,true);
			addEventListener(MouseEvent.MOUSE_DOWN,downState,false,0,true);
			addEventListener(MouseEvent.ROLL_OVER,overState,false,0,true);
			addEventListener(MouseEvent.ROLL_OUT,outState,false,0,true);
			addEventListener(MouseEvent.CLICK,clickState,false,0,true);

			addChild(label);
			//label.mouseEnabled=false;



			label.width=this.width-10;
			label.wordWrap=true;
			label.x=5*scaleFactor;
			label.multiline=true;
			label.autoSize=TextAlign.CENTER;
			label.selectable = false;
			var format:TextFormat = new TextFormat();
			if(centreText)
			{
				format.align = "center";
			}

			format.font = "Arial";
			format.color = 0xFFFFFF;
			trace("scaleFactor= "+scaleFactor)
			format.size = scaleFactor*_textSize;




			label.defaultTextFormat = format;
			trace("label.height="+label.height);
		}


		
		public function get correctAns():Boolean
		{
			return _correctAns;
		}

		public function set correctAns(value:Boolean):void
		{
			_correctAns = value;
		}



		protected function clickState(event:MouseEvent):void
		{
			if(centreText)
			{
				dispatchEvent(new Event(CLICKED));
				removeEventListener(MouseEvent.MOUSE_UP, upState);
				removeEventListener(MouseEvent.MOUSE_DOWN, downState);
				removeEventListener(MouseEvent.ROLL_OVER, overState);
				removeEventListener(MouseEvent.ROLL_OUT, outState);
			}
			
		}	
		
		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function setText(text:String):void
		{
			label.htmlText=text;
            labelText=text;



            while(label.height>backGround.height)
            {
                var format:TextFormat = new TextFormat();
                if(centreText)
                {
                    format.align = "center";
                }

                format.font = "Arial";
                format.color = 0xFFFFFF;
                trace("scaleFactor= "+scaleFactor);
                _textSize--;
                format.size = scaleFactor*_textSize;

                label.height=10;
                label.htmlText=text;


                label.defaultTextFormat = format;
            }

			label.y=(this.height/2)-(label.height/2)-(3);
		}

        public function setInvisibletext(text:String):void
        {
            labelText=text;

            //label.y=(this.height/2)-(label.height/2)-(3);
        }
		public function getText():String
		{
			return labelText;
		}
		
		public function overState(e:MouseEvent=null):void
		{
			
			if(!butUP)
			{
				blueDown.visible=true;
				blueUp.visible=false;
			}
			
		}
		public function outState(e:MouseEvent=null):void
		{
			
			if(!butUP)
			{
				blueDown.visible=false;
				blueUp.visible=true;
			}
			
		}

        public function makeMeGreen():void
        {

            greenDown.visible=false;
            greenUp.visible=true;
            if(!this.contains(greenUp))
            {

                backGround.addChild(greenUp);
            }
        }

        public function makeMeGreenDown():void
        {
            greenUp.visible=false;
            greenDown.visible=true;
            if(!this.contains(greenDown))
            {

                backGround.addChild(greenDown);
            }
        }
		
		public function upState(e:MouseEvent=null):void
		{
			blueDown.visible=false;
			blueUp.visible=true;
			butUP=true;
			if(!centreText)
			{
				if(_correctAns==true)
				{
					var greenUp:GreenButtonUp=new GreenButtonUp();
					var tick:Tick=new Tick();
					backGround.addChild(greenUp);
					tick.scaleX=scaleFactor;
					tick.scaleY=scaleFactor;
					addChild(tick);

                    tick.x=(_fatcontroller.deviceSize.width/100)*91;
                    tick.y=(backGround.height/2.6)

					removeEventListener(MouseEvent.MOUSE_UP, upState);
					removeEventListener(MouseEvent.MOUSE_DOWN, downState);
					removeEventListener(MouseEvent.ROLL_OVER, overState);
					removeEventListener(MouseEvent.ROLL_OUT, outState);
					
				}
				else
				{
					var redUp:RedButtonUp=new RedButtonUp();
					backGround.addChild(redUp);
					var cross:Cross=new Cross();
					cross.scaleX=scaleFactor;
					cross.scaleY=scaleFactor;
					addChild(cross);
					cross.x=(_fatcontroller.deviceSize.width/100)*91;
					cross.y=(backGround.height/2.6)
				}
				
				dispatchEvent(new Event(FIRED));
			}
		}
		
		public function downState(e:MouseEvent=null):void
		{



                blueDown.visible=true;
                blueUp.visible=false;
                butUP=false;

		}
	}
}