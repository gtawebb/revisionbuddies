package controller
{
import com.distriqt.extension.zzish.ZzishActivity;
import com.distriqt.extension.zzish.ZzishUser;
import com.milkmangames.nativeextensions.GoViral;

import flash.geom.Rectangle;
	
	import model.CheckNetworkConnection;
	import model.CollectionOfBoardPapers;
	import model.Constants;
	import model.DataPersistence;
	import model.HTTPService;
	import model.PastPaperLoader;
	
	import valueObjects.QuestionState;
	
	
	
	public class RevisionBuddiesFatController
	{
		
		private var _theCurrentView:String;;
		private var _questionNumber:int = 0;
		private var _historicQuestionNumber:int = 0;
		private var _score:int = 0;
		private var _questionData:Array;
		private var _sectionsData:Array;
		private var _resultsPercentage:int = 0;
		private var _storeKitStarted:Boolean = false;
		private var _httpService:HTTPService;
		private var _networkMonitor:CheckNetworkConnection;
		private var _detail:String="waiting to be set";
		private var _dataPersistence:DataPersistence;
		private var _myPurchases:Array=[];
		private var _dataBaseLastUpdate:Date;
		private var _details:Array;
		private var _sectionTitle:String="waiting to be set";
		private var _sectionDisplayName:String;
		private var _historicStates:Array;
		private var _storeKitInitialised:Boolean = false;
		private var _questionAlreadyStarted:Boolean = false;
		private var _presentState:QuestionState = new QuestionState();
		private var _deviceSize:Rectangle;
		private var _scaleFactorX:Number;
		private var _scaleFactorY:Number;




		private var _crossXYValues:Array = [];
		private var _crossFinalYValues:Array = [];
		private var _OCRPapers:CollectionOfBoardPapers;
		private var _AQAPapers:CollectionOfBoardPapers;
		private var _EdexcelPapers:CollectionOfBoardPapers;
		private var _pastPapersLoader:PastPaperLoader;
		private var _sectionsDownloaded:Boolean = false;
		private var _suddenDeath:Boolean = false;
        private var _significantEventAble:Boolean = true;
        private var _saveResultToGraph:Boolean=true;
        private var _zzishUser:ZzishUser;
        private var _zzishActivity:ZzishActivity;
        private var _zzishClassCode:String="notSet";




		public function RevisionBuddiesFatController()
		{
			//_networkMonitor = new CheckNetworkConnection();
			_dataPersistence = new DataPersistence();
			
			_historicStates=[];
          GoViral.create();


            if(GoViral.goViral.isFacebookSupported()) {
                GoViral.goViral.initFacebook(Constants.FACEBOOK_ID, '');
            }


		}
		
		

		public function get suddenDeath():Boolean
		{
			return _suddenDeath;
		}

		public function set suddenDeath(value:Boolean):void
		{
			_suddenDeath = value;
		}

		public function get sectionsDownloaded():Boolean
		{
			return _sectionsDownloaded;
		}

		public function set sectionsDownloaded(value:Boolean):void
		{
			_sectionsDownloaded = value;
		}

		public function get pastPapersLoader():PastPaperLoader
		{
			if(_pastPapersLoader == null)
			{
				_pastPapersLoader = new PastPaperLoader();
			}
			return _pastPapersLoader;
		}

		public function get EdexcelPapers():CollectionOfBoardPapers
		{
			return _EdexcelPapers;
		}

		public function set EdexcelPapers(value:CollectionOfBoardPapers):void
		{
			_EdexcelPapers = value;
		}

		public function get AQAPapers():CollectionOfBoardPapers
		{
			return _AQAPapers;
		}

		public function set AQAPapers(value:CollectionOfBoardPapers):void
		{
			_AQAPapers = value;
		}

		public function get OCRPapers():CollectionOfBoardPapers
		{
			return _OCRPapers;
		}

		public function set OCRPapers(value:CollectionOfBoardPapers):void
		{
			_OCRPapers = value;
		}

		public function get scaleFactorY():Number
		{
			return _scaleFactorY;
		}

		public function set scaleFactorY(value:Number):void
		{
			_scaleFactorY = value;
		}

		/*public function get crossFinalYValues():Array
		{
			return _crossFinalYValues;
		}

		

		public function get crossXYValues():Array
		{
			return _crossXYValues;
		}
		
		public function set crossXYValues(value:Array):void
		{
			_crossXYValues = value;
			trace("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!p   set _crossXYValues  = "+_crossXYValues)
			
		}*/
		
		
		
		
		
		
		public function get scaleFactorX():Number
		{
			return _scaleFactorX;
		}

		public function set scaleFactorX(value:Number):void
		{
			_scaleFactorX = value;
		}

		public function get deviceSize():Rectangle
		{
			return _deviceSize;
		}

		public function set deviceSize(value:Rectangle):void
		{
			_deviceSize = value;
		}

		public function get theCurrentView():String
		{
			return _theCurrentView;
		}

		public function set theCurrentView(value:String):void
		{
			_theCurrentView = value;
		}

		public function get presentState():QuestionState
		{
			return _presentState;
		}

		public function set presentState(value:QuestionState):void
		{
			_presentState = value;
		}

		public function get questionAlreadyStarted():Boolean
		{
			return _questionAlreadyStarted;
		}

		public function set questionAlreadyStarted(value:Boolean):void
		{
			_questionAlreadyStarted = value;
		}

		public function get sectionDisplayName():String
		{
			return _sectionDisplayName;
		}

		public function set sectionDisplayName(value:String):void
		{
			_sectionDisplayName = value;
		}

		public function get storeKitInitialised():Boolean
		{
			return _storeKitInitialised;
		}

		public function set storeKitInitialised(value:Boolean):void
		{
			_storeKitInitialised = value;
		}

		public function get historicQuestionNumber():int
		{
			return _historicQuestionNumber;
		}

		public function set historicQuestionNumber(value:int):void
		{
			_historicQuestionNumber = value;
		}

		public function get historicStates():Array
		{
			return _historicStates;
		}

		public function set historicStates(value:Array):void
		{
			_historicStates = value;
		}

		public function get sectionTitle():String
		{
			return _sectionTitle;
		}

		public function set sectionTitle(value:String):void
		{
			_sectionTitle = value;
		}

		public function get details():Array
		{
			return _details;
		}

		public function set details(value:Array):void
		{
			_details = value;
		}

		public function get dataBaseLastUpdate():Date
		{
			return _dataBaseLastUpdate;
		}

		public function set dataBaseLastUpdate(value:Date):void
		{
			_dataBaseLastUpdate = value;
		}

		public function get dataPersistence():DataPersistence
		{
			return _dataPersistence;
		}

		public function get myPurchases():Array
		{
			
			return _myPurchases;
		}

		public function set myPurchases(value:Array):void
		{
			_myPurchases = value;
			
			
			
		}

		public function get detail():String
		{
			return _detail;
		}

		public function set detail(value:String):void
		{
			_detail = value;
		}

		public function get networkMonitor():CheckNetworkConnection
		{
			return _networkMonitor;
		}

		public function get httpService():HTTPService
		{
			if(_httpService == null)
			{
				_httpService = new HTTPService();
				_httpService.fatController = this;
			}
			return _httpService;
		}


		public function get resultsPercentage():int
		{
			return _resultsPercentage;
		}

		public function set resultsPercentage(value:int):void
		{
			_resultsPercentage = value;
		}

		public function get sectionsData():Array
		{
			return _sectionsData;
		}

		public function set sectionsData(value:Array):void
		{
			_sectionsData = value;
		}

		public function get score():int
		{
			return _score;
		}

		public function set score(value:int):void
		{
			_score = value;
		}

		public function get questionData():Array
		{
			return _questionData;
		}

		public function set questionData(value:Array):void
		{
			_questionData = value;
		}

		public function get questionNumber():int
		{
			return _questionNumber;
		}

		public function set questionNumber(value:int):void
		{
			_questionNumber = value;
		}

        public function get significantEventAble():Boolean {
            return _significantEventAble;
        }

        public function set significantEventAble(value:Boolean):void {
            _significantEventAble = value;
        }

        public function set saveResultToGraph(saveResultToGraph:Boolean):void {
            _saveResultToGraph = saveResultToGraph;
        }

        public function get saveResultToGraph():Boolean {
            return _saveResultToGraph;
        }

        public function get zzishUser():ZzishUser {
            return _zzishUser;
        }

        public function set zzishUser(value:ZzishUser):void {
            _zzishUser = value;
        }

        public function get zzishActivity():ZzishActivity {
            return _zzishActivity;
        }

        public function set zzishActivity(value:ZzishActivity):void {
            _zzishActivity = value;
        }

        public function get zzishClassCode():String {
            return _zzishClassCode;
        }

        public function set zzishClassCode(value:String):void {
            _zzishClassCode = value;
        }
    }
}