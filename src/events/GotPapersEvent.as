package events
{
	import flash.events.Event;

	public class GotPapersEvent extends Event
	{
		
		
		public static const GOT_PAPERS:String = "GOT_PAPERS";
	
		
		public var theBoard:String = "";
		
		public function GotPapersEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false):void
		
		{
			
			//we call the super class Event
			
			super(type, bubbles, cancelable);
			
		}

	}
}