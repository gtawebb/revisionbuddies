package model
{
	public class CollectionOfBoardPapers
	{
		private var _papers:Array;
		private var _board:String;

		public function get board():String
		{
			return _board;
		}

		public function set board(value:String):void
		{
			_board = value;
		}

		public function get papers():Array
		{
			return _papers;
		}

		public function set papers(value:Array):void
		{
			_papers = value;
		}

	}
}