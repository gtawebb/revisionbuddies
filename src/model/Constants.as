package model
{
import flash.system.Capabilities;

public class Constants
	{
		public static const PLATFORM_APPLE:String = "Apple";
		public static const PLATFORM_GOOGLE:String = "Google Play";
        public static var SCALE_Y:int = 1;
        public static var SCALE_X:int = 1;
        public static var AD_DELAY:int = 60000;

        public static const START_QUESTION_NUMBER:int = 0;
        public static const TEST_ALL_QUESTIONS_IN_ORDER:Boolean = false;
        public static const TEST_MATH:Boolean = false;
        public static const KILL_CACHE:Boolean = false;
        public static const ZZISH:Boolean = true;
        public static const IMAGE_URL:String = "http://www.gtawebb.co.uk/revisionBuddies/images/";

        public static var RESETTING_OPTIONS:Boolean = false;




    public static function platform():String
        {
            var os:String;
            if(Capabilities.version.substr(0,3) == "IOS")
            {
                os=PLATFORM_APPLE;
            }
            else
            {
                os=PLATFORM_GOOGLE;
            }
            return os;
        }

    public static function inAppKey():String
    {
        var thekey:String;

        switch (SERVICE){



            case "GCSEFrenchCombinedService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwe3pa3wuk0hlFeJP4VH8btNV3T/TnZfqv1aJVcQPxs1mmHt8CzQvlZq4U5guc/MKKADFvSrhRWsYkecuCX7OE/GLZD4650iRCdwcb4KI90qYapRT3B6KWDP3Tda+LiVQqSgvgPLEOQpE9iwbtMtO/qbScTT2/8L1pwZsf0gpnlljL2fS2trjB474F85TPY4kAGw7EjKyC0ydioXS9J8W9ekFCWw6EGE5R+2i9giguyWnlkR3zgzT/J4BkowJJbMbTxIiJS1Bx4LOzu4GcjlH/lKUn7hIR1fjAUCx+nQDFHDF2cKW6eZ+cTX8X0Q68OwcJCgnwgYHJt1tDzoNwoa83QIDAQAB";
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj3LI4ZZ2gXlsMhTWX6kuq6bLbdkMLdxHxgq9hCSy+5OzOAz2slDQwNheQ4vEF6CrH7jJmM+tf5bNCY9fv6bhVpxAjxFqhxnnh5Dbx8kRo69RRZrMxH2nccu1FlaQP1j4CFy4dkHgzNaSOXCp+LGeIsaH3iLg3qtFMbNSTNMHfdyC2It9QOhvHYJRMGR1AEruugage0CH8J0u/RkVYa8IL9f1q8CUqBCqELufr/7K8iPrulQLjhkFCBXqUHM37Keyrkp4/GnqUSvB01JuZnLIXb36eCD2Vc3m1l5eQlNAPsuCcNdWo3o+xqqzV53oRZe5bnKrUPqxBxxMMyfD2YHVIQIDAQAB";
                }
                break;

            case "GCSEPhysicsService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvj35AViZLSLZ5i472kkkEfx8MVrLOcPly+0V5XGQneFdrmhaugZ+KEU7yQWHGfxuYl77wGvHmotmVtW5Ngd0wJc5kJz3fedLnhgpl0P8nkt22pVtU1Tr/Jjy/W0ur4Jie1BkNhaUSmlC3rz+6Q/ntDVUnJa261ZfMqemmG3sFAjNV8DacyzQSmdXBBKojabNxVf/BI8BKZbB6kU8g6UriTPOVXxKqlV5Krj6QnO7tfauqTYnvDVVxQTTwWCgsF7QhKPAfHv1muU11sc4lRInmRhoNLCoR6OsoT7Qr082gmxYNnCOABm0d5qNAw/eAUTufcDeIBNZLiKuvuF6BdVGKwIDAQAB"
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl+xPgKtHcgmMqq1BfMQUO1DBEW35Se3Na0On1D5+6wASc72QYCB1cRouZ2qJzubHRwzogUXu2MqluV5kPkyPa30fqSWHSR0qR2cvTBkKa/kRcdXESmPakoi46KH9FH2/O85KKfpwKAE/ZsIWSoJPZUQrorqxatt4bqIU4dyJpk1oR5rQzCUVp+/cFP8Zrv2RxCnwqzr514FU4AKigWdV0PjFU4RpiZz7LitZMPTWX14SgZEd37dKGMRV8wxFTjVHFar97X1AsoKX/M24RipkvrnkeAWoBeIK/ldv5IoanP5vY18nFht4NRtMob5E9504CDwZoL5KG6tCM28/Fu/UvQIDAQAB";

                }
                break;

            case "GCSERS":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApWXGKKSzbNs8eKP3ui+1jGMyaWGIJbC0g6o5gLeY88zMP40MJDDeGbOwoQuQGz2YbnLsXv+SZ+5i3FfC+LsxcBHQ/Ri1mNfOXgrPgreS2UdrxX9Xq3/E9YLVcB6bS/C+mxJeCNy0FeeJmP8iMc/CyCjdgUI1KJSuBoUwRbjORtAzFqh6e+K3uugT9cfaowO+4iy37E5mp2UH3MNhuEyk4mDCQkKP/vOmhTTmW2vmQ4aTnFPw9BLKozWIHfxft0fq9NX3boGnhgmoyAriSbJo3ePGk2nTAH/fpAW8riCMb6iLlC7fgyw0UhmHi56nWjj895tbqLbIWqPHw4zocfv8MQIDAQAB"
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjQ/WBdPm7tVet5rgxMGIhswkLD7JZWdFDGK3WOsIfZm21cyCXSUNK8OngIBCJbRGchP0U87IirNasmUGlHKBiWUIOvVTQYCcfarqCbzF/3/Ikd2OZPEQ/JcLDvuJEougzxEXiCsnRgFgbqKNQIkYiNTWXBKjTzMu91mzaBJc57qKIbYzjEx3YYU059bLbVctFePIJJZMmS+dz6mV/mfdK0o92Ys/h05cnMQwuFX460PmVxVRYlCq25xf3dIRGyelPFvXzk6v1L57Btdxft2dwhPUlpsdlPvqDQ1paDk0S998QJaoyMd9LOClMXTWbsllxwzeSCuzax8DdLuvdTtcBQIDAQAB";

                }
                break;

            case "GCSEHistoryService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn2jfTCiwwpKh3b6jgc+XxxuE4wh8XP4/qZuIgDl6bSed2NrbJK0tGukLDBUlxRTYe56FbHw3psRhE6tTPzFleF4KsTc9FEpye3ZJMT4G16W48zzX6htC07yaoC6GexA/+ATkiivEBZ7qD3okdd0TYg5cPEyKTa0gO2JCsq9SLMXQ/rLFEztU8t2/JrvHmCnoykk22l6qmSIfqC1MeWdSie+D2T80UZUPIVsF0eLJCPNmWdqNl6IVTgheSC40nkj/oIWXLOQBvB0jyMQ2BhFKJMgRk9H1JLWRdeCsOVTeAp4eZ+odK1vJ2QoqcFrDT6kjPik7ErUMjbNb3k88X/hclwIDAQAB"
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArnH/Sk0bcOZMh2cK2xCWqGuAjNARZoKW9z0+l591il+Yb/gwCLsamVR/zTnSPccGYD82fwuyH6N89EC2ZS07QymiEB+4P6st5N9wQZa0ex/61GDpBANTqNdvgyVqwsnxQuqY/PsPzNGtwMtfqR28sovSNL9MFQZQrkb8xrYgCEHPPJrrvacjlhYic9JgtmLiqZsmHf4KHCyN0+rSgWhtHVF9wCCgd96EOPWjTx/wtABfJfGgC6vwA494+oF2yffiIVzVP+wDtx+NUPwYPP6cRTx5IukYrfRdwJgXKhg5nvn2tfhzSjaaVpT5EPrYR9a51s0PIdAleGKgNetwcYzP4wIDAQAB";

                }
                break;

            case "GCSEGeographyService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtOHNC4Ed4VMlNuU+S7qdGlT3Opd6TS4dJHF6ROyL9/VLQXJK6hp3KzrpuZpsxGJbgsxRT3C4jvn6Tybr9jgsnijT2+E8ujHcacFxfoVjAoupusjRwvzzeul9coApSF+cBUYBtwGeK96PB4eAvd202ES2+SbjqKpr6EKLjAfuMGU6Ijf/aVfKaiB47/no1CXgJpiwVsxpgu3nojLCJVsrORideEThmcf6QS2sKiyE3daldUsXDSrphKP2rgyIorC4MjbL0x/Vc92ZfxP0g1dys/qasVuxCCZOwPh/DF2BHkVcTNnYz1VmmOua6ISXp7VdYUAxutbI8zZp/3BpArOG4wIDAQAB"                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArrYv8uPK3UNPZfM1xNLEjpRuAOBtkb+OZhQSPIT8qTiAjMVFdqlxvCUAtRdiI+6zYT5nP2kSdlwSe/26YJHnrzPmCZzEmmN6xP1lSIiJhXO6MNLfhsSTWbjCgpN/b0y7XkE82pOKUkW8QQ7VJA9gdiQzgNAyOf5Dnirjmdag16Mq8wW9C4vJNqjApDmZ2LSAWY+a59fQqQj0Q2Ew7t5cL712XI9IRcFVT0GPFLVZDrLov1/caix3EbvOqICslukCrkPVn5SfdU0QWnTWlCOlsOBP5s8Tk3cB0in7fYkTwSakL9bobGOAMfVwiU4VoHPXyjxSPt1mtZakx1nz7KEsAQIDAQAB";

                }
                break;

            case "GCSEMathsService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAllsuDlHtfwqamsTRTB0luyi6NVikuzOa2Xzpf2uknd6lqG64u7KeJtqwG1r1BPuk49S2D8ZSyawcSCYRfmOfRM7rfD438s/YVWbkZkJK8cqCauAPnTgR8fn9TqDK+UM7ugXNbkOgxqXnRzl5vWm+vkmNJdBniE712kF5LrepuDfKQ6ZrPB5LpiBKhwdS4sKV2HasfSMyy6fhgeJUZ/YcHHFiTD/WS4Z5NYIe9qjinzw6UNok8Q449neIamwuw2gwQXnE1T75WV9bgZX3dNc+jCRl3cCoUELUV/2lTlWANodZclPmmPiIZdzgwOFI1+ESEVR7KEtadlqzkbM6h4PAaQIDAQAB";
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAob032IhoZoGmxV33tHI8WrpxH0JA3tgfRh4Zs3Az2dO62FrcKegBuiEP3j/Q41+xsgoVzh2IEXtBFzIlmbXRZPSRCG5HzKMXqOS/D84JHmoDsIIT2r/VmCO4XtbArOxrwweVAi7LmZz2w/cmrmdLjlUan9dfen8I+9f9pAiAUZHovI7ogZlJKMmYqibfu6j1oChjmK2j3X1D9pJLXSzGle6MjJPAdlb9UpSYEfI+0WZeZf15gnaWEG2fJv/g0Qw8dKgTUY2qAJYEe3Ohxt/HNPhiTcplJo+fzosVmSXACiT7VfnRwbNRBwfb8KGOCu0rnFgGsVv2WlWuWqv5Q+sgWQIDAQAB";

                }
                break;
            case "GCSEBiologyService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjGFbW5KRtD542OCgUML3WlPO+Yy99PPZ3NW1C5nxh1p4GgZpMSQ/c6dUr7G6sdnPdRmg98utY3cHxaFRBMwYGw937aScyKwMQB3u3j5V0OGXTmGNhkMEK6ZDT6SG4g4Jw158lLkE+1CzxeMh3+JqWSq+R7lCAnJjjQE8BNto4Vy7jnFMFVhcejIlayKveMTW/+6taZSHDBaxpgXN/2ErEldK4jr+HYLs1CBArv7bno7LzAROXnP5D/Zvyli/tSaZzEpAzv94CBSDfQRA5DM1qkVQVByl2lrJDwQznbrVKKNYu6ZcjAmG76jya4isMcIvKTSMa6TkbnM5HVmeP9BD5QIDAQAB";
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAixK3uwCnxFwvn24hlpDdqspnHyewjwnhNorMzIdRKyxiJkKqbR7nFmiJtdGpR8QOZU0AI/Nn5GUBnR2gQ8k7NnqtGKFETpJogGL58/Eep7TcjN0dZp9NeT2QCl/4mm6/Q8aIIzsXSefFf1ZB8cR3RIC70lWTx1G2XJWwED0YNqAIOs71y6K1cVXaXnqfTBZfkqoHw1BLcFubgXBfYCX8YyvJrHlstvQjWTV8XO/cjGRu2AoBdxolxVo+8p0LLYVb9lU2MmGGZ8a1+9DwLJC9v+9uh0BE5SKzN4vMz1ZE7W3tv+hrEsySy45LbJ13hyBIKkoIdfpcWAmyoEWeghdNvQIDAQAB";

        }
                break;
            case "GCSEChemistryService":
                if(Constants.COMPLETE)
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiS4OepeezID4DDxUuxPawQZCINnTuHKBSp14VM5oB2sYVuX0yuk2yNndsUVQAS+QFx3USMJ5PIFXgCKULcEp94STFgtVM1mKG9/m1ijfdrvGY0PCf7MCrUg3Y8xKITamsXk0x4LT6vXrTKzgMGor+QbRI859W0rQeqfeAuJPxPFNv5acgNBY1kJpxCkufVqApN4Hcn45a+LlLsN1O/1N7hpJtP4ve5FD+RHZcCD6fHoBZooilmzTQxqCOVwxPl8BZanvsVSeMTJ4JCd5V5FdmOariXuhHatoIKW3xu/0ljDx5bleA/m5tFtS5ljhc9BJDyGnxQKloK0x5fVSIfw94QIDAQAB";
                }
                else
                {
                    thekey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAigkjqOtsa6xLaKVT3mLl7tHJ7oOkvP3vvYkWhCdvKYW10qQ5UBim7xp/2WrbFYLuyjJ2GUTR7mPGuzwu1/qn54dxWbX1gbvwJkTyhUxV03tavxMZd4HLhe+mdCxvysZQpQ2BxRqNIbFDLMJkqFSbTSG+VQqbOSUck4UXGqoQgzmRmN52aQ8FfIstzUnzWfNHjHZtxGRvtFiBgYKfLYVumYQq1mYtRtHRWosqPNpbNC4JfY7s9Pp8bbpkUesMwUmpaHQiJcmGJ9J/sJQwWM0wv7BrhPDR/gZKQ6giXzLyjV+Gp2Rpg0XTluRJ819kyBlm4yQG5+XzS6BbGzc7+HsyrwIDAQAB";

                }
                break;

        }
        return thekey;
    }

    public static function zzishKey():String
    {
        var thekey:String;

        trace("getting zzish ky, service ="+SERVICE)

        switch (SERVICE){
            case "GCSEFrenchCombinedService":
                    thekey="Nol6T3BarO3HuV9fT4PLPMh4cgIa";
                break;
            case "GCSEPhysicsService":
                   thekey="nQQlxgw3MfUBytQpP75nyfW70fEa";
                break;
            case "GCSERS":
                    thekey="Jvl4knxlkhqRJMZ2EjbK027nqUoa";
                break;
            case "GCSEHistoryService":
                    thekey="7t6N6a7WrxYcIZWxskhjzCq8Yfka";
                break;
            case "GCSEGeographyService":
                    thekey="5jHDXguWtdzYymh7MOTTevHUfNEa";
                break;
            case "GCSEMathsService":
                    thekey="SYdlHcwTo2mYRvh4hoSGVqbmXAAa";
                break;
            case "GCSEBiologyService":
                    thekey="G90T4bXkjZt7amYzlliml7BaE4sa";
                break;
            case "GCSEChemistryService":
                    thekey="K6ofhSdpCIaD_yC1WyAtaoTSFfYa";
                break;
        }
        return thekey;
    }

    public static function distriqtKey():String
    {
        var thekey:String;

        switch (APP_IDENTIFIER){
            case "com.revisionbuddies.gcsefrenchcombinedcompletely":
                thekey="10794190ecf05c485ae8263fe23d659684f72f39YP/G7APO6ertiaMFe4Ze1GGRSU+EROqhK6iCqvQkgycG0x0R+RWpwUZolmmc39RIGMroycaK2xhBt4MeS2JJm1jEMyxsuwiudN8OjmaREtQjEGhnBz4pdApFXX6R7N8Ugt90yjXIwz7S3TJBPLrzsAng38EKyM+Eo2qz9b4k+Cw1KfYvdIbqnZgn79h6BzPbA54163gu0rZamDl99SD9H+/CuW4t0copaBiX3Lh26SlvoqpSSkDMwQQS2+XchVHtznAjPXvwlhFjQy46eEUzzwBa+fsAL65+C5wCVVMGtQrN7jyJS/VtPf6tntMT5XcrmAK6VzSqbfo3U48CV1nimQ==";
                break;
            case "com.revisionbuddies.physics":
                thekey="d449f115ccca557a49634f9e90c466f6f5819fdbfoSY0QTXQ0D0pWJ/xuwxZ/I1sL3OIHK9O2uAezhTAUUfIPQywfbpGsk4hvyT9I5a8ozLMEqFMmxx5+b2PTY8YUaxc6DovipD7efoN2+SaZM5qEa1Of2YSGisILWGte8pTQrn5J01HWsIjxK2k/InTmIueWaheTK1u4msiMeTrmoYIk1/0jU8qnSrANkA6u7M0e5W92xWGCuqsBNrKGBZvFWOg4L4J3S+akvjygLUvGaItHt1LQlOUQ8fBliz+eG+QDZ8SkwbWGd8EtV/3tatMKkkEhFgGs5F89LdAUlltkMEL2L9nrTuQZKiwy0I8H8yvyi3HXCMLPZMjaZFEOBmfg==";
                break;
            case "com.revisionbuddies.gcsereligiousstudies":
                thekey="ae84cca03e85db250a8c978f5bc206280f3e9b25Wjqmpu+JME92spQffel2BIKNpcT/7cl+V4OkcQX6tnw9oaoY7xBj2UYM4+Y6o/r4vx7VcWiTBHo3bCNV8kRHg0f8AQbUHx+ykjG6RDKIUJJIaDvOUl4rG03kuf7qtX/OY+dhFIudYdBKdu0JlzcQOYOf/UCon/aCreiCx+HowXllEPxdCZDnPHpU+u/XtaORIg1sRCdpX2oLV4xzLc8jJ6YwhcnyKHlQZnEJ1bqKaOlo/oBFcT5F4Vt2RTr1czH6hgmSZg95d48SVngUlOoGW884df2TKqDceLqVkVDPArTliFKoFR6VXqm+Ea6V5d7AMr/3JJ/+VhjYEPEE0I8+vA==";
                break;
            case "com.revisionbuddies.historycomplete123":
                thekey="3d1c74211aeabfd382e8563db230eb9e1201592fcDapENtbcqBM5VOaVpEgpkGPTVbM/eOfhk0KnPNFkrabGQREQmHBQ5G18tbnWy+stztmsKekWZJFst7AhO/kUzNI977TnM1eiPT+eKxheesPfrwg2Qx9Z+htkWgdJfDaJOTwODhhTiWCWFaZgJstcFZkrKQR0uxwti1KbRnnlzaHl9IYqJYzoWxVxB1ICt6CNdTcKooUpvL7njbRjh4ZjCEO64aClifS57tXWW+crSyJHw9LDkAgODhSKf26IAFvgdALtve8BLB6aHLxkzCrt4g/lcgavnxbmRz13gHhE6u5p/tO8rFsK3wvRVt6VGPezw49b2yyknLPalH4nc6+LQ==";
                break;
            case "com.revisionbuddies.gcsegeography":
                thekey="82ce7e361fdf2c431e26f23f922ea3a1986745daCFKJAi74792Sxs/IM+/ek6wW59+4Njm4EUQVI8DLuBfrvxiRMKMyYM+rIHMzRQV90Gj0Rct+Z1l0JRqn62pee5IS9W6TSVga7Fd/9b5am5hTPV4szrR90JmEv6ekrt1B07ikdqXszejWaN/wcbxr/8E68p/CRHCeQjcQw8JtCuCr6pUIFk10ZtGDhtjA13GQFdtbjKKDN9Xvjid/M16ywUiwNL/KjOspORO9sZqEHcGD2nNQ/WZYKyL/sBydWGW7mRf0i7UN9DNDWTluSRp9tngXO+muzPCqqIhg0Q1KFyhFWX+Jf/7oaAsK/JtXnnjsXmg5kCnsc1fKFZr7O91jHw==";
                break;
            case "com.revisionbuddies.gcsemaths":
                thekey="5f9221bc94cbc2a2e561a487f6b1039dbd0a75d1RvVfjKPWbdSadxKHvmGrvwIFl9id/JHTrtOSBe5tc7wmf7lGge2jZW2upoUrSsOIP7IrFcSnjnyDs4Tw2iEIRv2Lgg4HOKd2VllyoIKx55/J48SXeJsR1+9waC6lvj8HFVi3GetCEveXXEVxUG3lrzJ4t+PW/2a2A9VrFa0VfpdDj4YimWQk4A2ktfN1mqsTcK/smkgIxDPDwGfOlkzBE4GK63iyInS5Xpu1X05CuO6HOTvrcFwUlwdqtxLqfJGEWXpi2WLvTip2hE+P+uSeLCP0l82ZzteVlFxiZoaC794hGw+zA/kQgqI157IKzS2XsJK2NrYZ0oCx7lkz+X2mjw==";
                break;
            case "com.revisionbuddies.biologycomplete":
                thekey="6ef816512c1e78deeb525df0bfd2134491f6bbf1P6CetgIqhxmEjJutzbsahErz3RHxyNowWGfaVltgHmbmd4TQMqmqrfDPzb0xmDm2ZhS9RFr9xpVZXQ3YUzlb6P4HnhzIeADOyJrhrBWESw/433jYr/jV8hwo6+rLpvP9/K/NFYlmWuCN36F1bYypMlo1It8XAswNoF5J30E0/0rDhYIWPqK2wGgwns/4h/pl6LnL0aHWC8moDXShTXjckS8Hbp6Lafl4H80PXt/LMUnWh+r7a8Um4Ax2yT27S0AAvX5jxCLcsm0D9Sr3Xaij8xbD6aK7AyT1JfZ9ZD5+D+JwXgP1xWh5zlfiFb5zrrLDpUJhjdd39mTMyULJ4KNiNQ==";
                break;
            case "com.revisionbuddies.chemistry":
                thekey="232ffd6c5d83b92cdfbb4de37a569ffea1a140ceAkuzLpnX640KXgkOYPiqh3Uw97b7YimFSFHTjwUvxoW4xRxkvZy5WEZSMrZsKORVxFbBl9YxxYZHHDsmxko0WZVLJufDbv9oRRr3+X6mJx+4HtbQWvtOXDd+k3Y786EBljt4cuZhsuOufg0N9t08mZRQ65u+a2WFaF755ohbp+hbvjtEnLGEVrObr1xr8lsRgvyfXbH9g0I7BGwnL2kQwsHH53RSCEnEtZYcqN3VelmVk8gqf5nBJ3w3lS5mJyPAx0gEGYFOrcx92SHtE1e6SuwHrJio7/cvBas6xu9KRfiQeoNrBcQmwqvAu2JSEuj946WUZTiq7tPmreRqPDBA8w==";
                break;
            case "com.revisionbuddies.gcsefrenchcombined":
                thekey="1193d75ebe576ec2535ed472aea35ac20e76c34cV5wxduOfrr7+7SZoLsIsbR04PSD71Jh5d5BuTwJVIh8+wEevvMoRw8VFz7eVzrVx42LgoPHJEZ85TrmkvCt2/euBo6t/I4HX67EeL86gL5wjpkEN9Yhs8mhp4qVpo8r1u1agTnl5PUWLb3BpOLbKxBPDiMily9um/Ld52BeeAgwYA5FJiM37SMtl/GDhA7OiapPaqkhPOgDhEZzRbAumzXSVqYGExnbnaLtRIX69dNubsXN2JFVbkRhuqslE5BMJ5G6lstuKBu5x+OhI4tqqQRihV+rh1IVfyk6T0gEjFE2wIPE/9rpvm9ZlgpdDmvZLR3PN+DTa4dzOQj2nOHJj7w==";
                break;
            case "com.revisionbuddies.physicscompletely":
                thekey="3afb4ae7c7db0b203b1ff10800e819aed83e43c7GYbhW7zBOvC7vkiia18ReNzzj+cA05VdBMi3ZLwMcjtm/PvyBvWvY3ljYIvNu27YIZggwY9s3CHLPL+kiLN2IRGZwhNp5AJdwGY4H3lxLINGwZUxv3tWM7wVBtZZn0kV1HzBpcvyh7Hz1cYwD2nhpbXUbJf+fAixxOd2sOFgXQ32TP3G/1L/5a0J31XfaY+ia9CCJQ2zxWavFDDFE5IsxPA+FTiEprEVGIpLAbJzi/AqUTRdH3b/hOmEeL/A5J1EknQaxnftOWl4I46SNjsYdN5JbBBA/FU2aN0wNamXwGM5UzbVYW6WzQnFKkXhooxJV69lxe0ZYx3tf9TzJNISDQ==";
                break;
            case "com.revisionbuddies.rscomplete":
                thekey="25d344f0aa4051b3192c4f49a2f9527a64e230c4CgX/JzhoCKhV4f+fZRGCu62RIwJ+zBoWF1uKq7bhVxTxP5dAZWQKC8x2phERXApYMK4zNrTah4j0z8EIPfYwQDBA08KVcctxKpacdNrHWZC8jcKSYsTahMgkHQ9/OeXg5E4EtOtG72pAd802sX7It5x3m9WmsTfUGV+9HDkZ0QiQOk/pEjB2ZAI5puhQAvG1aqHsGSGzJerVJxg3fKbiPBv42Vjds6CHx0VknLRqMBCJgsYRFlqdci8ZXQBn7f63GC9D6ZOxKkVN4ZZEZDN9BV24Cbwldqtg/MBZWKoMCwai4Mavc8dzIy+zo+xGbTPoHGNARDP9lPSsBiqeu4h8oA==";
                break;
            case "com.revisionbuddies.gcsehistory":
                thekey="11cf0d014d37b004f0854a0be566a2ebb480ef36VunE2xHJrXrmhqfL+lPjVSw1EmyUR+SU1urYVTnGbLHwlFzJaouG++hWcl3Ts+NcXci1wRgtCQ2vxADW52hIy0sI4QZ8AZqfkiD7fqSacbo8X5SR8ujSCeMmrsi4gCtGumhYffm27JAR1cOZVRRJ3rdpa2ZigCBQWMTjuxlrBICtUbSoAjoMalsftfiB74yF9T9rT9aA6MF9qLvSv+Vd1aQGHJ11lHXN77fuRIH4Fg5fWHEgWeQQVcqdoy24Zg/6XsJA7ww6y7SOMOFJB5Jm2bvjAGpNhHvRkbWCu+nO0ZI50WGbyPBOOIiWT7zUp5jJ3fJjA7pcjlgZLLYFUBesoQ==";
                break;
            case "com.revisionbuddies.geographycomplete":
                thekey="e85238b9a2437bb010bbe4884a67c63f3d5faa19DrfyZxi/hovjw6xGeVeQozdyXFOiZZoUdXfVIggE3Ci8my4emhz5hyTkjFQ1il7tn7rd/ZgyQ2CP2H3HSy7ORH2IkZhTfg5b7Px9uE4rXmLwGFHCHxvb6A9volvviqaTetcktldAVuYEPhz4C3d9qyGjPLkBZCQ0IohNzYaCOrWBq6esyCmqnrUqNdrPlzxnoRc9bIiRd89o2byoIX5gloD1E0cOM2VR2vsj8lK6irMX9Xtse6nh5/fON3WDAfEtWvP71nl1CzYtHmvURJLb4avmhBAImGu5NlN7kNJRR7N3lKtgqfQ7KYYkn1iazZkGgSzb+e1u4vHADLLDlodHKg==";
                break;
            case "com.revisionbuddies.gcsemathscomplete":
                thekey="b66b6ce023bf07c7eedbccf89822bdb8e56c3539aapBvazvNTeJa6qPfDRmSFUpiakpqPHdieQ5aACG+GJwTUL5CHPcCKtINIAS0ccBu/PAeN7w6lzOoLptQU2WAqHY6/wkn7+M9aGnrluV7J6eWX5jIIuSSi676VEGXLFDuWL6fPX66EWFKVe5lPMC9xI7MCMxfp1qDwVMRYKj7Z8coXRC67jOObtMpWtXG0BpJNuMcvd+5UJKmjteQg2Rx20wYdWrry2D+ylYwHTL/DjoQOAnSDK9l0LIl0F7p0jaybBujd3uXoN4vJd2j/A3JMYXxTZufLDziME6AMbWRZX8I/nl3BW4U+Tgp8Q7/rZeDt0ohZb++nzuA8yJqTpQLA==";
                break;
            case "om.revisionbuddies.biology":
                thekey="4c22ae468b1cc6b09ec91bab3d67dc4d57b896ebWSb35x6toxIxrvnc8pEKgkqjvRwjcLUL6DEDKOJzD2SNtwE340Gqz1nncLl8G3V8KVnjoZpLZ0dfmbkYZSmiB7gQIW2+4eudozzveRlAUStwN49yBtuZURxwMcGo96f+vyEEtEov75IKzTQ5457Em5j9hxzaPLmmiVBX94vbz737iubqMl+37rQGmL8XAkQzQnH7hEyOIw4GfiUuBPjvMV236v14QJVaGiftvq7Sf0bOWPblrVnj10bTFJ+ol5TuRTuy4K9QGplR6iD9NglWN+z5Apn1KSnaC+Asby+u3JoMXAThY5vcuJiPQOA9U1UINzp+08ccEFnJE54QnexBkw==";
                break;
            case "com.revisionbuddies.chemistrycomplete":
                thekey="3b1807e13c50aab6ffaf2c9e4cce602ae21e318caCEqxlEf03jEC1eEeAEt4Bap/DJJtVFA2SlwW33ffpU6JnGbJu2jPs+oNaSe44gGLIAlz6UIbKWwUkuW2EHSmcu7rJmgBVKy9xpBLIk0+45CkcFbBuQqmZr41D36H/mqKesZFJAjsVZy5pHk+bW/nvqj9+8x3yIWQ6XSzVRe0o7CpbTXloKfD+bzXSIRlwI8BJsN//Lip4YFchJJJ/phs95gRe5xftCgrALaq8MhXOgc4sOsuaylkB/utxV6coi1DvukF1+4soonp7Xqt78fSCTuUi8sHC/j3aK1TDLXOJCkwsjTIAmCFz/GkzuBCcb4GDXii6vsED8Pl/IeJA+GSw==";
                break;
        }
        return thekey;
    }
		
		
		//foundation french
		/*public static const APP_NAME:String = "GCSE French - Foundation";
		public static const SYLLABUS:String = "covering the subject and grammar requirements of AQA, Edexcel and OCR GCSE French - Foundation syllabuses.";
		public static const SERVICE:String = "GCSEFrenchFoundationService";
		public static const NUMBER_OF_QUESTIONS:String = "1000";
		public static const FACEBOOK_ID:String = "465891603462516";
		public static const PLATFORM:String = "Apple";
		public static const APPLE_APP_ID:String = "574293848";
		public static const URBAN_AIRSHIP_APP_ID:String = "sPwTnK8bSG2-5VBVhWjwRw";//dev
		public static const URBAN_AIRSHIP_APP_SECRET:String = "zKu2-t7SRMiSyg1lBBd--A";//dev
		//public static const URBAN_AIRSHIP_APP_ID:String = "TdOAKE2XTiaAHBzUhgFXrA";//prod
		//public static const URBAN_AIRSHIP_APP_SECRET:String = "jA4caTi9SgqRKwNsv8sduw";//prod
		*/
		
		//higher french
		/*public static const APP_NAME:String = "GCSE French - Higher";
		public static const SYLLABUS:String = "covering the subject and grammar requirements of AQA, Edexcel and OCR GCSE French - Higher syllabuses.";
		public static const SERVICE:String = "GCSEFrenchHigherService";
		public static const NUMBER_OF_QUESTIONS:String = "1000";
		public static const FACEBOOK_ID:String = "435247643190936";
		public static const PLATFORM:String = "Apple";
		public static const APPLE_APP_ID:String = "578654982";
		public static const ADMOB_ACCOUNT_ID:String = "ca-app-pub-4569728198419870/2160041687";
		public static const URBAN_AIRSHIP_APP_ID:String = "X3q7d724Qy-rXxYsp8B68Q";//dev
		public static const URBAN_AIRSHIP_APP_SECRET:String = "c1AfAC2-RLimQqAG8qceOg";//dev
		//public static const URBAN_AIRSHIP_APP_ID:String = "v8A9lDBoTKegdQ9VQ6EO9A";//prod
		//public static const URBAN_AIRSHIP_APP_SECRET:String = "NUFh2RvKSxyMxf_qVJFW-Q";//prod
		*/
		
		
		//history
		public static var APP_NAME:String = "";
		public static var SERVICE:String = "";
		public static var SYLLABUS:String = "";
		public static var NUMBER_OF_QUESTIONS:String = "";
		public static var FACEBOOK_ID:String = "";
		//public static const PLATFORM:String = "Google Play";

		public static var APPLE_APP_ID:String = "";
        public static var APP_IDENTIFIER:String = "";


		public static var ADMOB_ACCOUNT_ID:String = "";
		//public static const URBAN_AIRSHIP_APP_ID:String = "VKftMMHJRx6pLKkQaWDuLQ";//dev
		//public static const URBAN_AIRSHIP_APP_SECRET:String = "36Mn0VG5SUCDJoObYv2f1g";//dev
		public static var URBAN_AIRSHIP_APP_ID:String = "";//prod
		public static var URBAN_AIRSHIP_APP_SECRET:String = "";//prod
		public static var IMAGE_FOLDER:String = "";
        public static var COMPLETE:Boolean = false;

        public static var INITIAL_OPTIONS:Boolean=false;
        public static var BOARD_OPTIONS:Array=[];
        public static var TIER_OPTIONS1:Array=[];
        public static var TIER_OPTIONS2:Array=[];


        public static var BOARD_CHOICE:String = "";
        public static var TIER1_CHOICE:String = "";
        public static var TIER2_CHOICE:String = "";
        public static var CHOSEN_OPTIONS_ARRAY:Array = [];








    //maths
    /*public static const IMAGE_FOLDER:String = "images_maths/";
    public static const APP_NAME:String = "GCSE Maths";
    public static const SERVICE:String = "GCSEMathsService";
    public static const SYLLABUS:String = "covering the majority of the GCSE Maths Syllabus for each of the Edexcel, OCR and AQA exam boards";
    public static const NUMBER_OF_QUESTIONS:String = "1500";
    public static const FACEBOOK_ID:String = "122943014524516";
    public static const PLATFORM:String = "Google Play"
    public static const ADMOB_ACCOUNT_ID:String = "ca-app-pub-4569728198419870/1109320483";
    public static const URBAN_AIRSHIP_APP_ID:String = "FoU6TaGCQeW0PgRLQ28BPg";//dev
    public static const URBAN_AIRSHIP_APP_SECRET:String = "JB8DlvbWRbKYcQoDXkQDdg";*///dev
		//public static const URBAN_AIRSHIP_APP_ID:String = "z7QvtJsORR-tXcx1AR4_1Q";//prod
		//public static const URBAN_AIRSHIP_APP_SECRET:String = "4bAGb4aYQx6K2oFwpdnVhQ";//prod
		
		//RS
		/*public static const IMAGE_FOLDER:String = "images_rs/"
		public static const APP_NAME:String = "GCSE Religious Studies";
		public static const SERVICE:String = "GCSERS";
		public static const SYLLABUS:String = "covering the majority of the AQA, Edexcel and OCR GCSE Religious Studies syllabuses.";
		public static const NUMBER_OF_QUESTIONS:String = "1000";
		public static const FACEBOOK_ID:String = "390578724351099";
		public static const APPLE_APP_ID:String = "580863000"
		public static const PLATFORM:String = "Google Play";
		public static const ADMOB_ACCOUNT_ID:String = "ca-app-pub-4569728198419870/1109320483";
		public static const URBAN_AIRSHIP_APP_ID:String = "Rz5L6U1VQmCqoahCNE0RFA";//dev
		public static const URBAN_AIRSHIP_APP_SECRET:String = "8T59kY57QO6Xks-Twt9bdw";//dev
		//public static const URBAN_AIRSHIP_APP_ID:String = "CXAwoDDCQLqfSyX4gsP8gg";//prod
		//public static const URBAN_AIRSHIP_APP_SECRET:String = "PX8m9cYUTNmpGRY_nc2AHw";//prod
		*/
		
	}
}