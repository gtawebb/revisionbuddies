package model
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import views.RevisionBuddiesHomeView;

	public class DataPersistence
	{
		public function readData(filename:String):Array
		{
			trace("my saved object file name = "+filename);
			var file:File = File.applicationStorageDirectory.resolvePath(filename);
			var myCategories:Object;
			
			if (!file.exists)
			{	
				trace("the file doesn't exist")
				myArray = [];
			}
			else
			{
				var fileStream:FileStream = new FileStream();
				fileStream.open(file, FileMode.READ);
				myCategories = fileStream.readObject();
				trace("my saved object in amf format = "+myCategories);
				trace("my saved object valueOf = "+myCategories.valueOf());
				trace("my saved object value = "+myCategories.value);
				
				var myArray:Array = [];
				for (var i:int = 0; i < myCategories.value.length; i++)
				{
					myArray[i] = myCategories.value[i];
				}
				trace("the file exists, myArray= "+myArray)
				fileStream.close();
			}
			return myArray;
		}
		
		
		
		public function saveData(data:Array, filename:String):void {
			
			trace("saving data, filename = "+filename)
			trace("saving data, data = "+data)
			
			var object:Object = new Object();//create an object to store
			object.value =  data; //set the text field value to the value property
			//create a file under the application storage folder
			var file:File = File.applicationStorageDirectory.resolvePath(filename);
			if (file.exists)
				file.deleteFile();
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);// and open the file for write
			fileStream.writeObject(object);//write the object to the file
			
			fileStream.open(file, FileMode.READ);
			
			fileStream.close();
		}
	}
}