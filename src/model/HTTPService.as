package model
{

import com.distriqt.extension.dialog.Dialog;

import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NetStatusEvent;
	import flash.net.NetConnection;
	import flash.net.Responder;
	
	import controller.RevisionBuddiesFatController;
	
	import events.GotPapersEvent;

import flash.net.URLRequest;

import views.RevisionBuddiesHomeView;

	public class HTTPService extends EventDispatcher
	{
		
		private const GATEWAY:String = "http://www.gtawebb.co.uk/revisionBuddies/bin-debug/gateway.php";
		private var connection:NetConnection;
		private var questionsResponder:Responder;
		private var sectionResponder:Responder;
		private var edexcelPapersResponder:Responder;
		private var ocrPapersResponder:Responder;
		private var aqaPapersResponder:Responder;
		private var lastUpdateResponder:Responder;
		private var detailsResponder:Responder;
		private var _fatController:RevisionBuddiesFatController;
		private var _makingLastUpdateCall:Boolean = false;


		public static const NO_CONNECTION:String = "NO_CONNECTION";
		public static const POPULATE_SECTIONS:String = "POPULATE_SECTIONS";
		public static const POPULATE_DETAILS:String = "POPULATE_DETAILS";

		public static const DETAILS_FILENAME:String = "DETAILS_FILENAME";

        import flash.net.URLLoader;
        import flash.net.URLVariables;
        import flash.events.Event;




        public function set fatController(value:RevisionBuddiesFatController):void
		{
			_fatController = value;

		}

		public function get fatController():RevisionBuddiesFatController
		{
			return _fatController;
		}

		public function setUp(questionsHandler:Function, lastUpdateHandler:Function):void
		{
			trace("!!!!!!!!!!!!!!!!!!!!      CONNECTION = "+connection)
            trace("!!!!!!!!!!!!!!!!!!!!      loading config")
			if(connection == null)
			{


				connection = new NetConnection;
				connection.httpIdleTimeout = 5000;
				connection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
				connection.connect(GATEWAY)


			}


			sectionResponder = new Responder(getSections_resultHandler, onFault);
			questionsResponder = new Responder(questionsHandler, onFault);
			lastUpdateResponder = new Responder(lastUpdateHandler, onFault);
			detailsResponder = new Responder(getDetailsHandler, onFault);
			edexcelPapersResponder = new Responder(getEdexcelPapersHandler, onFault);
			ocrPapersResponder = new Responder(getOcrPapersHandler, onFault);
			aqaPapersResponder = new Responder(getAqaPapersHandler, onFault);
		}


		private function netStatusHandler(event:NetStatusEvent):void
        {
			trace("NETW_STATUS_HANDLER !!!!!  event = "+event.info.code);
			switch (event.info.code)
            {
				case "NetConnection.Connect.Failed":
				case "NetConnection.Connect.Closed":
				case "NetConnection.Call.Failed":
				case "NetConnection.Call.BadVersion":
					showNoConnectionPage();
					break;
			}
		}


		public function makeQuestionsCall(params:String):void
		{
			_makingLastUpdateCall = false;



           // connection.call(Constants.SERVICE+".getQuestionsMinBySection", questionsResponder, params, Constants.TIER1_CHOICE, Constants.TIER2_CHOICE);
            trace("Constants.BOARD_CHOICE = "+Constants.BOARD_CHOICE.toUpperCase());

            trace("Constants.TIER1_CHOICE = "+Constants.TIER1_CHOICE.toUpperCase());
            trace("Constants.TIER2_CHOICE = "+Constants.TIER2_CHOICE.toUpperCase());

          //  trace("Constants.TIER_OPTIONS2 = ["+Constants.TIER_OPTIONS2[0].toUpperCase()+" , "+Constants.TIER_OPTIONS2[1].toUpperCase()+"]");

            if(Constants.BOARD_CHOICE==""&&Constants.TIER1_CHOICE==""&&Constants.TIER2_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getQuestionsMinBySection", questionsResponder, params);

            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE==""&&Constants.BOARD_CHOICE=="")
            {
                trace("question with tier1 only option (french)")
                connection.call(Constants.SERVICE+".getQuestionsMinBySection", questionsResponder, params, Constants.TIER1_CHOICE.toUpperCase(),"","" );

            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE=="")
            {
                trace("question with tier1 AND board only option ")

                connection.call(Constants.SERVICE+".getQuestionsMinBySection", questionsResponder, params, Constants.BOARD_CHOICE.toUpperCase(),Constants.TIER1_CHOICE.toUpperCase(),"","" );

            }
            //if tier2 = lower, get just lower
            else if (Constants.TIER2_CHOICE==Constants.TIER_OPTIONS2[1])
            {
                trace("question with tier1 AND board AND tier2, with tier2 being lower")

                connection.call(Constants.SERVICE+".getQuestionsMinBySection", questionsResponder, params, Constants.BOARD_CHOICE.toUpperCase(), Constants.TIER1_CHOICE.toUpperCase(), Constants.TIER2_CHOICE.toUpperCase(),"" );
                trace("prams = "+params, Constants.TIER1_CHOICE.toUpperCase())
            }
            //if tier2 = higher get higher and lower
            else if (Constants.TIER2_CHOICE==Constants.TIER_OPTIONS2[0])
            {
                trace("question with tier1 AND board AND tier2, with tier2 being higher")

                connection.call(Constants.SERVICE+".getQuestionsMinBySection", questionsResponder, params, Constants.BOARD_CHOICE.toUpperCase(), Constants.TIER1_CHOICE.toUpperCase(), Constants.TIER2_CHOICE.toUpperCase(), Constants.TIER_OPTIONS2[1]);
                trace("prams = "+params, Constants.TIER1_CHOICE.toUpperCase(), Constants.TIER2_CHOICE.toUpperCase())
            }



		}
		public function makeEdexcelPapersCall():void
		{
			_makingLastUpdateCall = false;
            if(Constants.TIER1_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getPapers", edexcelPapersResponder,"edexcel");
            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getPapers", edexcelPapersResponder,"edexcel",Constants.TIER1_CHOICE.toUpperCase());

            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE!="")
            {
                connection.call(Constants.SERVICE+".getPapers", edexcelPapersResponder,"edexcel",Constants.TIER1_CHOICE.toUpperCase(),Constants.TIER2_CHOICE.toUpperCase());

            }



		}
		public function makeOcrPapersCall():void
		{
			_makingLastUpdateCall = false;

            if(Constants.TIER1_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getPapers", ocrPapersResponder,"ocr");
            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getPapers", ocrPapersResponder,"ocr",Constants.TIER1_CHOICE.toUpperCase());

            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE!="")
            {
                connection.call(Constants.SERVICE+".getPapers", ocrPapersResponder,"ocr",Constants.TIER1_CHOICE.toUpperCase(),Constants.TIER2_CHOICE.toUpperCase());

            }


		}
		public function makeAqaPapersCall():void
		{
			_makingLastUpdateCall = false;
            trace("Constants.TIER1_CHOICE = "+Constants.TIER1_CHOICE)
            trace("Constants.TIER2_CHOICE = "+Constants.TIER2_CHOICE)


            if(Constants.TIER1_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getPapers", aqaPapersResponder,"aqa");
            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE=="")
            {
                connection.call(Constants.SERVICE+".getPapers", aqaPapersResponder,"aqa",Constants.TIER1_CHOICE.toUpperCase());

            }
            else if(Constants.TIER1_CHOICE!=""&&Constants.TIER2_CHOICE!="")
            {
                trace("making call for aqa, with the two tier options");

                connection.call(Constants.SERVICE+".getPapers", aqaPapersResponder,"aqa",Constants.TIER1_CHOICE.toUpperCase(),Constants.TIER2_CHOICE.toUpperCase());

            }


		}

		protected function getDetailsHandler(object:Object):void
		{

			_fatController.detail = object[0].DESCRIPTION;
			//trace("_fatController.detail1 = "+_fatController.detail)

			trace("!!!!!!!!  POPULATE_DETAILS  dispatching the event")
			dispatchEvent(new Event(POPULATE_DETAILS));
		}

		protected function getSections_resultHandler(object:Object = null):void
		{
			var sectionData:Array = [];
			for (var i:int = 0; i < object.length; i++)
			{
				sectionData[i] = object[i];
			}
			fatController.sectionsData = sectionData;
			
			var myDate:Date = new Date();
			
			var dateArray:Array = [myDate];
			
			getDataPersistenceManager().saveData(dateArray, RevisionBuddiesHomeView.CATEGORIES_UPDATE_FILENAME);
			getDataPersistenceManager().saveData(sectionData, RevisionBuddiesHomeView.CATEGORIES_FILENAME);
			trace("dispatching the POPULATE_SECTIONS")
			dispatchEvent(new Event(POPULATE_SECTIONS));
		}
		
		public function getEdexcelPapersHandler(object:Object = null):void
		{
			var papersData:Array = [];
			trace("!!!!sectionData = "+papersData)
			for (var i:int = 0; i < object.length; i++) 
			{
				papersData[i] = object[i];
			}
			var collectionOfBoardpaper:CollectionOfBoardPapers = new CollectionOfBoardPapers();
			collectionOfBoardpaper.papers = fatController.pastPapersLoader.createPapers(papersData,"edexcel");
			collectionOfBoardpaper.board = "Edexcel";
			_fatController.EdexcelPapers = collectionOfBoardpaper;
			var gotPapersEvent:GotPapersEvent = new GotPapersEvent(GotPapersEvent.GOT_PAPERS);
			gotPapersEvent.theBoard = "Edexcel";
			dispatchEvent(gotPapersEvent);
			
		}
		public function getAqaPapersHandler(object:Object = null):void
		{
			var papersData:Array = [];
			trace("!!!!sectionData = "+papersData)
			for (var i:int = 0; i < object.length; i++) 
			{
				papersData[i] = object[i];
			}
			var collectionOfBoardpaper:CollectionOfBoardPapers = new CollectionOfBoardPapers();
			collectionOfBoardpaper.papers = fatController.pastPapersLoader.createPapers(papersData,"aqa");
			collectionOfBoardpaper.board = "AQA";
			_fatController.AQAPapers = collectionOfBoardpaper;
			var gotPapersEvent:GotPapersEvent = new GotPapersEvent(GotPapersEvent.GOT_PAPERS);
			gotPapersEvent.theBoard = "AQA";
			dispatchEvent(gotPapersEvent);
			
			
			
				
		}
		public function getOcrPapersHandler(object:Object = null):void
		{
			var papersData:Array = [];
			trace("!!!!sectionData = "+papersData)
			for (var i:int = 0; i < object.length; i++) 
			{
				papersData[i] = object[i];
			}
			var collectionOfBoardpaper:CollectionOfBoardPapers = new CollectionOfBoardPapers();
			collectionOfBoardpaper.papers = fatController.pastPapersLoader.createPapers(papersData,"ocr");
			collectionOfBoardpaper.board = "OCR";
			_fatController.OCRPapers = collectionOfBoardpaper;
			var gotPapersEvent:GotPapersEvent = new GotPapersEvent(GotPapersEvent.GOT_PAPERS);
			gotPapersEvent.theBoard = "OCR";
			dispatchEvent(gotPapersEvent);
			
		
		}
		
		private function getDataPersistenceManager():DataPersistence
		{
			return fatController.dataPersistence;
		}
		
		private function showNoConnectionPage():void
		{
			trace("showNoConnectionPage, _makingLastUpdateCall = "+_makingLastUpdateCall)
			if(_makingLastUpdateCall)
			{
				trace("LAST UPDATE CALL FAILED");
			}
			else
			{
				trace("!!!!!!!!!!!!!!   SHOW THE NO CINNECTION PAGE");
				//dispatchEvent(new Event(NO_CONNECTION));
                Dialog.service.showAlertDialog(0, "Connection error", "Your device appears to have lost it's connection to the internet, please try again when you have a wireless or 3G data connection", "Close");

            }
		}
		
		
		public function actuallyMakeSectionsCall():void
		{
			_makingLastUpdateCall = false;
			trace("actuallyMakeSectionsCall()")
            trace("Constants.SERVICE= "+Constants.SERVICE);
            trace("Constants.TIER1_CHOICE= "+Constants.TIER1_CHOICE);
            trace("Constants.BOARD_CHOICE= "+Constants.BOARD_CHOICE);
            if(Constants.BOARD_CHOICE!="")
            {
                if(Constants.TIER1_CHOICE!="")
                {
                    //used for board option and tier1
                    trace("calling getSectionsByBoardAndTier1(), board="+Constants.BOARD_CHOICE.toUpperCase()+", tier1="+Constants.TIER1_CHOICE.toUpperCase());
                    connection.call(Constants.SERVICE+".getSectionsByBoardAndTier1", sectionResponder, Constants.BOARD_CHOICE.toUpperCase(), Constants.TIER1_CHOICE.toUpperCase());
                }
                else
                {
                    trace("getSectionsByBoard");

                    //used for just board option
                    connection.call(Constants.SERVICE+".getSectionsByBoard", sectionResponder, Constants.BOARD_CHOICE.toUpperCase());
                }

            }
            else if(Constants.BOARD_CHOICE==""&&Constants.TIER1_CHOICE!="")
            {
                //used for just tier1 option

                trace("getSectionsByTier1, Constants.TIER1_CHOICE.toUpperCase="+Constants.TIER1_CHOICE.toUpperCase()+", SERVICE = "+Constants.SERVICE+".getSectionsByTier1");
                connection.call(Constants.SERVICE+".getSectionsByTier1", sectionResponder, Constants.TIER1_CHOICE.toUpperCase());

            }

            else
            {
                //used for no options
                connection.call(Constants.SERVICE+".getQuestionsSections", sectionResponder);
            }
		}
	
		
		public function makeLastUpdateCall():void
		{
			//if(_interentAvailable)
			trace("making last update call")
			_makingLastUpdateCall = true;
			connection.call(Constants.SERVICE+".getLastUpdate", lastUpdateResponder);
		}
		
		public function makeDetailsCall(section:String):void
		{
			//if(_interentAvailable)
			trace("!!!!!!!!!!!!!!!!!    makeDetailsCall")
			_makingLastUpdateCall = false;
			connection.call(Constants.SERVICE+".getDetailsBy", detailsResponder, section);
		}
		
		public function onFault (object:Object):void
		{
			trace("we have a fault")
		}
	}
}