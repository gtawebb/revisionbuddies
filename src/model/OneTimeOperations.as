/**
 * Created by George on 14/05/2014.
 */
package model {
import com.distriqt.extension.zzish.Zzish;
import com.distriqt.extension.zzish.ZzishActivity;
import com.distriqt.extension.zzish.ZzishUser;
import com.distriqt.extension.zzish.events.GenericEvent;
import com.distriqt.extension.zzish.events.ZzishEvent;

import controller.RevisionBuddiesFatController;

import flash.desktop.NativeApplication;
import flash.events.EventDispatcher;

import model.OneTimeOperations;

import mx.core.FlexGlobals;

CONFIG::adverts
{
    import com.distriqt.extension.adverts.AdvertPlatform;
    import com.distriqt.extension.adverts.AdvertPosition;
    import com.distriqt.extension.adverts.Adverts;
    import com.distriqt.extension.adverts.events.AdvertEvent;
}


import com.distriqt.extension.dialog.Dialog;
import com.distriqt.extension.dialog.events.DialogEvent;

import com.adobe.air.sampleextensions.android.licensing.*;


import flash.display.MovieClip;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

import flash.events.TimerEvent;
import flash.utils.Timer;
import flash.utils.clearInterval;

import views.RevisionBuddiesHomeView;

public class OneTimeOperations extends EventDispatcher
{

    private var myTimer:Object;

    private var readyToShowAds:Boolean = false;

    private static var _instance:OneTimeOperations = null;

    private var lc:LicenseChecker = new LicenseChecker();

    public static const DISABLE_UI = "DISABLE_UI";
    public static const CLASS_CODE_ACCEPTED = "CLASS_CODE_ACCEPTED";
    public static const DIALOG_CLOSED = "DIALOG_CLOSED";
    //public static const ENABLE_UI = "ENABLE_UI";


    public function OneTimeOperations()
    {

        initDialog();
        if(Constants.platform()==Constants.PLATFORM_GOOGLE && Constants.COMPLETE==true)
        {
            if(!Constants.KILL_CACHE)
            {
                //checkLicense();
            }
        }
        if (CONFIG::adverts)
        {
            initAds();
        }

        if(Constants.ZZISH==true)
        {
            trace("setting up zzish");
            setUpZzish();
        }



    }

    private function setUpZzish():void
    {
        try
        {
            message( "Zzish Supported: " + Zzish.isSupported );
            message( "Zzish Version:   " + Zzish.service.version );


            Zzish.service.addEventListener( GenericEvent.ERROR, zzish_generalError );
            Zzish.service.addEventListener( "startApplication", zzish_initHandler );
            Zzish.service.addEventListener( "validateClassCode", zzish_initHandler );
            Zzish.service.addEventListener( "saveUser", zzish_initHandler );
            Zzish.service.addEventListener( "startActivity",   zzish_initHandler );
            Zzish.service.addEventListener( "cancelActivity", zzish_initHandler );
            Zzish.service.addEventListener( "stopActivity", zzish_initHandler );
            Zzish.service.addEventListener( "saveAction", zzish_initHandler );
            trace("zzish key = "+Constants.zzishKey());
            Zzish.service.startWithApplicationId( Constants.zzishKey());
        }
        catch (e:Error)
        {
            message( "ERROR::"+e.message );
        }



    }

    private function zzish_generalError( event:GenericEvent ):void
    {
        message( "genericError::"+event.message + "::" + event.type );
    }

    private function zzish_initHandler( event:ZzishEvent ):void
    {
        message( "zzish_initHandler::"+event.status + "::"+event.message + "::" + event.type );
        if(event.type=="validateClassCode"&&event.status!=200)
        {
            classCodeDialog();
        }
        else if(event.type=="validateClassCode"&&event.status==200)
        {
            trace("CLass code accepted")
            dispatchEvent(new Event(CLASS_CODE_ACCEPTED));
        }

    }




    public static function sharedInstance():OneTimeOperations {
        if (_instance == null) {
            _instance = new OneTimeOperations();
        }

        return _instance;
    }

    private function retryDialog(status:String, statusReason:String):void
    {

        Dialog.service.addEventListener( DialogEvent.DIALOG_CLOSED, dialog_dialogClosedHandler, false, 0, true );
        Dialog.service.addEventListener( DialogEvent.DIALOG_CANCELLED, dialog_dialogCancelledHandler, false, 0, true );

//status + " statusReason: " + statusReason,

        //Dialog.service.showMultipleChoiceDialog(2, "License check: " +status + ". Please purchase the app the Google Play store. Status: Reason: " + statusReason, "hello",  otherLabels);

        Dialog.service.showAlertDialog(2, "Licensing Error", "There has been a problem validating the license of this app. If this is your first time using the app, please restart it making sure that you have a data connection so that license verification can be carried out. Please also make sure that it has been purchased via the Google Play store via the Google Play User Account that is currently active on your phone." +
                    " Status: " + status + " Info: " + statusReason);

    }

    private function classCodeDialog():void
    {

        Dialog.service.addEventListener( DialogEvent.DIALOG_CLOSED, dialog_dialogClosedHandler, false, 0, true );
        Dialog.service.addEventListener( DialogEvent.DIALOG_CANCELLED, dialog_dialogCancelledHandler, false, 0, true );

//status + " statusReason: " + statusReason,

        //Dialog.service.showMultipleChoiceDialog(2, "License check: " +status + ". Please purchase the app the Google Play store. Status: Reason: " + statusReason, "hello",  otherLabels);

        Dialog.service.showAlertDialog(9, "Unrecognised Class Code", "Please check the class code that you entered and try again.");

    }

    private function dialog_dialogCancelledHandler( event:DialogEvent ):void
    {
        trace( "Results  Dialog Cancelled: id="+event.id +" data="+event.data );
    }

    private function dialog_dialogClosedHandler( event:DialogEvent ):void
    {
        trace( "Results Dialog Closed: id="+event.id +" button="+event.data );

        if(event.id==2){

            NativeApplication.nativeApplication.exit();
        }else if(event.id==9){
            dispatchEvent(new Event(DIALOG_CLOSED))
        }

    }

    protected function disableInterface():void
    {
        trace("dispatching enabling UI event")

        dispatchEvent(new Event(DISABLE_UI));
    }

//    protected function enableInterface():void
//    {
//
//        trace("dispatching disabling UI event")
//
//        dispatchEvent(new Event(ENABLE_UI));
//
//    }

    protected function checkLicense():void
    {

        lc.addEventListener(LicenseStatusEvent.STATUS,licenseResult);
        lc.setKey(Constants.inAppKey());
        lc.checkLicense();

    }



    protected function licenseResult(event:LicenseStatusEvent):void{

        trace("status: " + event.status + " statusReason: +" + event.statusReason);


        if (event.status == LicenseStatus.LICENSED) {

            //enableInterface();


            //Application is licensed, allow the user to proceed.
        } else if (event.status == LicenseStatus.NOT_LICENSED) {

            //Application is not licensed, don't allow to user to proceed.

            disableInterface();
            retryDialog(event.status,event.statusReason);



            switch (event.statusReason) {
                case LicenseStatusReason.CHECK_IN_PROGRESS:
                    // There is already a license check in progress.
                    break ;
                case LicenseStatusReason.INVALID_PACKAGE_NAME:
                    // Package Name of the application is not valid.
                    break ;
                case LicenseStatusReason.INVALID_PUBLIC_KEY:
                    // Public key specified is incorrect.
                    break ;
                case LicenseStatusReason.MISSING_PERMISSION:
                    // License Check permission in App descriptor is missing.
                    break ;
                case LicenseStatusReason.NON_MATCHING_UID:
                    // UID of the application is not matching.
                    break ;
                case LicenseStatusReason.NOT_MARKET_MANAGED:
                    // The application is not market managed.
                    break ;
                default:
                // Application is not licensed.
            }
        }
    }

   /* private function initAdBudz():void {

        if (Constants.platform() == Constants.ADBUDDIZ_APPLE) {
            AdBuddiz.setIOSPublisherKey(Constants.ADBUDDIZ_APPLE);

            trace("AdBuddiz.setIOSPublisherKey(Constants.ADBUDDIZ_APPLE);" + Constants.ADBUDDIZ_APPLE)

        } else {

            AdBuddiz.setAndroidPublisherKey(Constants.ADBUDDIZ_GOOGLE);
            trace("AdBuddiz.setAndroidPublisherKey(Constants.ADBUDDIZ_GOOGLE);" + Constants.ADBUDDIZ_GOOGLE)
        }
        AdBuddiz.setLogLevel(AdBuddizLogLevel.INFO);

        trace("AdBuddiz.cacheAds()")
        // AdBuddiz.setTestModeActive();
        AdBuddiz.cacheAds();

        AdBuddiz.addEventListener(AdBuddizEvent.didCacheAd, function ():void {
            trace("didCacheAd");
            AdBuddiz.logNative("didCacheAd");
        });
        AdBuddiz.addEventListener(AdBuddizEvent.didShowAd, function ():void {
            trace("didShowAd");
            AdBuddiz.logNative("didShowAd");
        });
        AdBuddiz.addEventListener(AdBuddizEvent.didFailToShowAd, function (e:AdBuddizEvent):void {
            trace("didFailToShowAd: " + e.error);
            AdBuddiz.logNative("didFailToShowAd: " + e.error);
        });
        AdBuddiz.addEventListener(AdBuddizEvent.didClick, function ():void {
            trace("didClick");
            AdBuddiz.logNative("didClick");
        });
        AdBuddiz.addEventListener(AdBuddizEvent.didHideAd, function ():void {
            trace("didHide");
            AdBuddiz.logNative("didHide");
        });
    }
*/
    CONFIG::adverts
    public function stage_clickHandler(event:MouseEvent):void {
        try {
            if (readyToShowAds) {
                Adverts.service.hideAdvert();

                trace("setting up a new ad timer")
                myTimer = new Timer(Constants.AD_DELAY, 1);
                myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, refresh);

                myTimer.start();
                readyToShowAds = false;
            }
            else {
                trace("not ready to show ads")
            }
        }
        catch (e:Error) {
            message("ERROR::" + e.message);
        }
    }

    private function initDialog():void {
        try {
            Dialog.init(RevisionBuddiesHomeView.DEV_KEY);

            message("Dialog Supported: " + String(Dialog.isSupported));
            message("Dialog Version: " + Dialog.service.version);

            //Dialog.service.addEventListener(DialogEvent.DIALOG_CLOSED, dialog_dialogClosedHandler, false, 0, true);
            //Dialog.service.addEventListener( DialogEvent.DIALOG_CANCELLED, dialog_dialogCancelledHandler, false, 0, true );
        }
        catch (e:Error) {
            message("ERROR::" + e.message);
        }
    }

//    private function dialog_dialogClosedHandler(event:DialogEvent):void {
//        message("Dialog Closed: id=" + event.id + " button=" + event.data);
//        clearInterval(progressInterval);
//    }
//
//    private var progressInterval:uint;
//    private var progress:Number = 0;
//
//    private function progressDialogIntervalHandler():void {
//        // Update the progress dialog
//        progress += 0.01;
//        Dialog.service.updateProgressDialog(1, progress);
//
////			clearInterval( progressInterval );
////			Dialog.service.dismissProgressDialog(1);
//    }

    CONFIG::adverts
    private function initAds():void {
        try {
            Adverts.init(RevisionBuddiesHomeView.DEV_KEY);

            message("Adverts Supported: " + Adverts.isSupported);
            message("Adverts Version:   " + Adverts.service.version);
            message("ADMOB Support = " + Adverts.service.isPlatformSupported(AdvertPlatform.PLATFORM_ADMOB));
            message("IAD Support = " + Adverts.service.isPlatformSupported(AdvertPlatform.PLATFORM_IAD));

            if (Adverts.isSupported) {
                Adverts.service.addEventListener(AdvertEvent.RECEIVED_AD, adverts_receivedAdHandler, false, 0, true);
                Adverts.service.addEventListener(AdvertEvent.ERROR, adverts_errorHandler, false, 0, true);
                Adverts.service.addEventListener(AdvertEvent.USER_EVENT_DISMISSED, adverts_userDismissedHandler, false, 0, true);
                Adverts.service.addEventListener(AdvertEvent.USER_EVENT_LEAVE, adverts_userLeaveHandler, false, 0, true);
                Adverts.service.addEventListener(AdvertEvent.USER_EVENT_SHOW_AD, adverts_userShowAdHandler, false, 0, true);

                //if (Adverts.service.isPlatformSupported( AdvertPlatform.PLATFORM_ADMOB ))
                if (Adverts.service.isPlatformSupported(AdvertPlatform.PLATFORM_IAD)) {
                    Adverts.service.initialisePlatform(AdvertPlatform.PLATFORM_IAD);
                    //Adverts.service.initialisePlatform( AdvertPlatform.PLATFORM_ADMOB, Constants.ADMOB_ACCOUNT_ID );


                    message("Adverts.starting count to show ads");

                    myTimer = new Timer(Constants.AD_DELAY, 1);
                    myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, startAds);
                    myTimer.start();


                }
                else {
                    message("iAd not supported, trying admob");
                    //Adverts.service.initialisePlatform( AdvertPlatform.PLATFORM_IAD);
                    Adverts.service.initialisePlatform(AdvertPlatform.PLATFORM_ADMOB, Constants.ADMOB_ACCOUNT_ID);


                    message("Adverts.starting count to show ads");

                    myTimer = new Timer(Constants.AD_DELAY, 1);
                    myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, startAds);
                    myTimer.start();
                }
            }
            else {
                message("Not supported...");
            }
        }
        catch (e:Error) {
            message(e.message);
        }
    }




    CONFIG::adverts
    private function startAds(e:Event = null):void {
        if(RevisionBuddiesHomeView.STOP_ADS != true)
        {

            myTimer.stop();
            myTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, startAds);
            var size:AdvertPosition = new AdvertPosition();
            size.verticalAlign = AdvertPosition.ALIGN_TOP;
            size.horizontalAlign = AdvertPosition.ALIGN_CENTER;
            Adverts.service.showAdvert(size);
            readyToShowAds = true;
            trace("starting ads")
        }
        else
        {
            myTimer.start();
        }

    }

    CONFIG::adverts
    private function refresh(e:Event = null):void {
        myTimer.stop();
        myTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, refresh);
        message("trying to refresh");
        readyToShowAds = true;
        if (Adverts.isSupported && RevisionBuddiesHomeView.STOP_ADS != true) {
            message("actually refreshing");
            Adverts.service.refreshAdvert();
            var size:AdvertPosition = new AdvertPosition();
            size.verticalAlign = AdvertPosition.ALIGN_TOP;
            size.horizontalAlign = AdvertPosition.ALIGN_CENTER;
            Adverts.service.showAdvert(size);


        }
        else {
            message("stopping ads");
            Adverts.service.hideAdvert();
            Adverts.service.removeEventListener(AdvertEvent.RECEIVED_AD, adverts_receivedAdHandler, false);
            Adverts.service.removeEventListener(AdvertEvent.ERROR, adverts_errorHandler, false);
            Adverts.service.removeEventListener(AdvertEvent.USER_EVENT_DISMISSED, adverts_userDismissedHandler, false);
            Adverts.service.removeEventListener(AdvertEvent.USER_EVENT_LEAVE, adverts_userLeaveHandler, false);
            Adverts.service.removeEventListener(AdvertEvent.USER_EVENT_SHOW_AD, adverts_userShowAdHandler, false);
        }
    }

    private function message(str:String):void {
        trace(str);
        //_text.appendText(str+"\n");
    }

    CONFIG::adverts
    private function adverts_receivedAdHandler(event:AdvertEvent):void {
        message("adverts_receivedAdHandler");
        myTimer.stop();
        readyToShowAds = true;
    }

    CONFIG::adverts
    private function adverts_errorHandler(event:AdvertEvent):void {
        message("adverts_errorHandler:: " + event.details.message);
        myTimer.stop();
        readyToShowAds = true;
        //
        //	If you wish you can force a reload attempt here by setting a delayed call to refreshAdvert
        //_timeoutID=setTimeout( refresh, 120000 );
    }

    CONFIG::adverts
    private function adverts_userDismissedHandler(event:AdvertEvent):void {
        message("adverts_userDismissedHandler");
        myTimer.stop();
        readyToShowAds = true;
    }

    CONFIG::adverts
    private function adverts_userLeaveHandler(event:AdvertEvent):void {
        message("adverts_userLeaveHandler");
        myTimer.stop();
        readyToShowAds = true;
    }

    CONFIG::adverts
    private function adverts_userShowAdHandler(event:AdvertEvent):void {
        message("adverts_userShowAdHandler");
        myTimer.stop();
        readyToShowAds = true;
    }
}
}
