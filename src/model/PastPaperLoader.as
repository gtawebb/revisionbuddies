package model
{

	import flash.display.Sprite;
	


	public class PastPaperLoader extends Sprite
	{
		private var collectionOfYears:Array = [];
		private var year:Year; 
		
		private var myData:Array = ["OCR_*_2011_*_CHRISTIANITY AND ISLAM – BELIEF0_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf0",
		"OCR_*_2011_*_CHRISTIANITY AND ISLAM – BELIEF1_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf1",
		"OCR_*_2011_*_CHRISTIANITY AND ISLAM – BELIEF2_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf2",
		"OCR_*_2010_*_CHRISTIANITY AND ISLAM – BELIEF3_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf3",
		"OCR_*_2010_*_CHRISTIANITY AND ISLAM – BELIEF4_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf4",
		"OCR_*_2010_*_CHRISTIANITY AND ISLAM – BELIEF5_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf5",
		"OCR_*_2010_*_CHRISTIANITY AND ISLAM – BELIEF6_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf6",
		"OCR_*_2010_*_CHRISTIANITY AND ISLAM – BELIEF7_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf7",
		"OCR_*_2009_*_CHRISTIANITY AND ISLAM – BELIEF8_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf8",
		"OCR_*_2009_*_CHRISTIANITY AND ISLAM – BELIEF9_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf9",
		"OCR_*_2009_*_CHRISTIANITY AND ISLAM – BELIEF10_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf10",
		"OCR_*_2009_*_CHRISTIANITY AND ISLAM – BELIEF11_*_http://www.ocr.org.uk/images/64568-question-paper-unit-b589-perspectives-on-world-religions.pdf11"]

		public function PastPaperLoader()
		{
		
		}
		
		
				
		public function createPapers(media:Array,board:String):Array {

            var uniqueYears:Array = [];
            collectionOfYears = [];

            var yearString:String;
            var boardString:String;
            var titleURL:String;
            var textValues:Array;
            //media = myData;
            //MonsterDebugger.trace(this,media,"mediadata");
            for (var i:int = media.length - 1; i >= 0; i--) {


                var myYear:Year = new Year();
                var paper:Paper = new Paper();
                //MonsterDebugger.trace(this,media[i]["value"],"item1");

                textValues = String(media[i][board]).split("_*_");
                boardString = textValues[0];
                yearString = textValues[1];
                paper.section = textValues[2];
                paper.title = textValues[3];
                paper.url = textValues[4];

                if (uniqueYears.lastIndexOf(yearString) == -1) {

                    uniqueYears.push(yearString);
                    myYear.addPaper(paper);
                    myYear.theDate = yearString;
                    collectionOfYears.push(myYear);
                }
                else {
                    for (var j:int = collectionOfYears.length - 1; j >= 0; j--) {

                        if (Year(collectionOfYears[j]).theDate == yearString) {
                            Year(collectionOfYears[j]).addPaper(paper);
                        }
                    }
                }
            }
            var collectionOfBoardPapers:CollectionOfBoardPapers = new CollectionOfBoardPapers();
            collectionOfBoardPapers.board = boardString;
            collectionOfBoardPapers.papers = collectionOfYears;

            return collectionOfYears;
        }
	}
}