package model
{
	

	public class ResultsMessages
	{
		private const t100a:String = "Top marks!"
		private const t100b:String = " You're a genius"
		private const t90a:String = "Great score!"
		private const t90b:String = " That was nearly perfect"
		private const t80a:String = "Well done!"
		private const t80b:String = " You really know your stuff"
		private const t70a:String = "Fantastic effort!"
		private const t70b:String = " You're coming on well"
		private const t60a:String = " Good work!"
		private const t60b:String = " Keep at it"
		private const t50a:String = " Not bad! "
		private const t50b:String = " But room for improvement"
		private const t40a:String = " Chin up!"
		private const t40b:String = " You can only get better"
		private const t30a:String = " Bad luck!"
		private const t30b:String = " Have another go"
		private const t20a:String = "Oh dear!"
		private const t20b:String = "We all have to start somewhere"
		private const t10a:String = "Ouch!"
		private const t10b:String = "Let's forget about that one"
		private const t0a:String = "Oh no!"
		private const t0b:String = "It's a big fat zero"
		
		
		public function GetResultsMessages(score:int):Array
		{
			score = roundToNearest(10, score);
			var message1:String = this["t"+score+"a"];
			var message2:String = this["t"+score+"b"];
			return [message1, message2];
		}
		
		private function roundToNearest(roundTo:Number, value:Number):Number
		{
			return Math.floor(value/roundTo) * roundTo;
		}
		
		

	}
}