package model
{
	public class Year
	{
		
		public var _papers:Array = [];
		private var _theDate:String;
		
		public function Year()
		{
		}

		public function get theDate():String
		{
			return _theDate;
		}

		public function set theDate(value:String):void
		{
			_theDate = value;
		}

		public function get papers():Array
		{
			return _papers;
		}

		public function addPaper(value:Paper):void
		{
			_papers.push(value);
		}

	}
}