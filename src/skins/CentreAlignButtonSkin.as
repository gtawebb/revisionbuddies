package skins
{
	import flash.text.TextFieldAutoSize;

	public class CentreAlignButtonSkin extends MCQButtonSkin
	{
		public function CentreAlignButtonSkin()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			
			labelDisplay.setStyle("textAlign","center");
			labelDisplay.autoSize = TextFieldAutoSize.CENTER;
			//labelDisplayShadow.autoSize = TextFieldAutoSize.CENTER;
	
		}
	}
}