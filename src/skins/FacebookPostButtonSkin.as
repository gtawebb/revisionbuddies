package skins
{
	import skins.assets.Facebook_post_icon_down;
	import skins.assets.Facebook_post_icon_up;
	


	public class FacebookPostButtonSkin extends MCQButtonSkin
	{
		public function FacebookPostButtonSkin()
		{
			super();
			upBorderSkin = Facebook_post_icon_up;
			downBorderSkin = Facebook_post_icon_down;
		}
	}
}