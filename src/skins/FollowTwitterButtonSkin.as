package skins
{
	import skins.assets.TwitterDown;
	import skins.assets.TwitterUp;

	public class FollowTwitterButtonSkin extends MCQButtonSkin
	{
		public function FollowTwitterButtonSkin()
		{
			super();
			upBorderSkin = TwitterUp;
			downBorderSkin = TwitterDown;
		}
	}
}