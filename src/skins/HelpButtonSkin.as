package skins
{
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

import flashx.textLayout.formats.LineBreak;

import mx.core.DPIClassification;
import mx.events.FlexEvent;

import skins.assets.GreenButtonUp;
import skins.assets.RedButtonUp;
import skins.assets.RoundedButton;
import skins.assets.help_btn;

import spark.components.Label;
import spark.components.supportClasses.Skin;
import spark.components.supportClasses.StyleableTextField;
import spark.skins.mobile.ButtonSkin;

public class HelpButtonSkin extends MyButtonSkin
{
    private var colorized:Boolean = false;
	private var myTextField:StyleableTextField;
	private var oldUnscaledWidth:Number;
    
    public function HelpButtonSkin()
    {
        super();
		if (applicationDPI == DPIClassification.DPI_320)
			oldUnscaledWidth = 640;
		else if (applicationDPI == DPIClassification.DPI_240)
			oldUnscaledWidth = 480
		else // 160 dpi
			oldUnscaledWidth = 320
		
		//super.labelDisplayShadow. = true;
        
        // replace FXG asset classes
        upBorderSkin = skins.assets.help_btn;
        downBorderSkin = skins.assets.help_btn;
		
        
        measuredDefaultHeight = 44;
        measuredDefaultWidth = 20;
		
			
		
	//trace("fdgadfg",)
    }
	
	override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void
	{
		// omit call to super.drawBackground() to apply tint instead and don't draw fill
		var chromeColor:uint = getStyle("chromeColor");
		
		if (colorized || (chromeColor != 0xDDDDDD))
		{
			// apply tint instead of fill
			//applyColorTransform(border, 0xDDDDDD, chromeColor);
			
			// if we restore to original color, unset colorized
			colorized = (chromeColor != 0xDDDDDD);
			//labelDisplay.truncateToFit(
		}
	}
  

	
	
	
	
	
	/**
	 * Upgrade measure to handle text reflow.
	 */
	
}
}
