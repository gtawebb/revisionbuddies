package skins
{
	import skins.assets.FacebookDown;
	import skins.assets.FacebookUp;
	

	public class LikeFacebookButtonSkin extends MCQButtonSkin
	{
		public function LikeFacebookButtonSkin()
		{
			super();
			upBorderSkin = FacebookUp;
			downBorderSkin = FacebookDown;
		}
	}
}