package skins
{
import flash.text.TextFieldAutoSize;

import mx.core.DPIClassification;

import spark.components.supportClasses.StyleableTextField;

import skins.assets.BlueButtonDown;
import skins.assets.BlueButtonUp;

public class MCQButtonSkin extends MyButtonSkin
{
    private var colorized:Boolean = false;
	private var myTextField:StyleableTextField;
    
    public function MCQButtonSkin() {
        super();
        var oldUnscaledWidth:Number;
        if (applicationDPI == DPIClassification.DPI_320)
            oldUnscaledWidth = 640;
        else if (applicationDPI == DPIClassification.DPI_240)
            oldUnscaledWidth = 480
        else // 160 dpi
            oldUnscaledWidth = 320

        //super.labelDisplayShadow.

        // replace FXG asset classes
        upBorderSkin = skins.assets.BlueButtonUp;
        downBorderSkin = skins.assets.BlueButtonDown;


        measuredDefaultHeight = 44;
        measuredDefaultWidth = 20;


        //trace("fdgadfg",)
    }
    
    override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void
    {
        // omit call to super.drawBackground() to apply tint instead and don't draw fill
        var chromeColor:uint = getStyle("chromeColor");
        
        if (colorized || (chromeColor != 0xDDDDDD))
        {
            // apply tint instead of fill
            applyColorTransform(border, 0xDDDDDD, chromeColor);
            
            // if we restore to original color, unset colorized
            colorized = (chromeColor != 0xDDDDDD);
			//labelDisplay.truncateToFit(
        }
    }
	
	override protected function createChildren():void
	{
		super.createChildren();
		labelDisplay.setStyle("color","0xFFFFFF");
		labelDisplay.setStyle("textAlign","left");
		labelDisplay.multiline = true;
		labelDisplay.wordWrap = true;
		labelDisplay.autoSize = TextFieldAutoSize.LEFT;
		
		/*labelDisplayShadow.setStyle("color","0x000000");
		labelDisplayShadow.x = labelDisplay.x+1
		labelDisplayShadow.y = labelDisplay.y+1
		labelDisplayShadow.setStyle("textAlign","left");
		labelDisplayShadow.multiline = true;
		labelDisplayShadow.wordWrap = true;
		labelDisplayShadow.autoSize = TextFieldAutoSize.LEFT;
		labelDisplayShadow.visible = false;
		
		labelDisplay.width = 100
		labelDisplayShadow.width = labelDisplay.width*/
	}
	
	
	
	
	
	/**
	 * Upgrade measure to handle text reflow.
	 */
	
}
}
