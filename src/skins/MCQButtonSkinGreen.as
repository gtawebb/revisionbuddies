package skins
{
	import skins.assets.GreenButtonUp;

	public class MCQButtonSkinGreen extends MCQButtonSkin
	{
		public function MCQButtonSkinGreen()
		{
			super();
			upBorderSkin = skins.assets.GreenButtonUp;
			downBorderSkin = skins.assets.GreenButtonUp;
		}
	}
}