package skins
{
	import mx.core.mx_internal;
	
	import spark.skins.mobile.ActionBarSkin;
	import spark.skins.mobile320.assets.ActionBarBackground;
	
	use namespace mx_internal;

	public class MyActionBarSkin extends spark.skins.mobile.ActionBarSkin
	{
		mx_internal static const ACTIONBAR_CHROME_COLOR_RATIOS:Array = [0, 80];
		public function MyActionBarSkin()
		{
			super();
			borderSize = 1;
			layoutShadowHeight = 3;
			layoutContentGroupHeight = 43;
			layoutTitleGroupHorizontalPadding = 13;
			
			borderClass = ActionBarBackground;
			
		}
		
		private var borderSize:uint;
	}
}
