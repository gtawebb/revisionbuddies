package skins
{
import skins.assets.RetryDown;
import skins.assets.RetryUp;



	public class RetryButtonSkin extends MCQButtonSkin
	{
		public function RetryButtonSkin()
		{
			super();
			upBorderSkin = RetryUp;
			downBorderSkin = RetryDown;
		}
	}
}