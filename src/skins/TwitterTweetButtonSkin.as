package skins
{
	import skins.assets.twitter_post_icon_down;
	import skins.assets.twitter_post_icon_up;
	


	public class TwitterTweetButtonSkin extends MCQButtonSkin
	{
		public function TwitterTweetButtonSkin()
		{
			super();
			upBorderSkin = twitter_post_icon_up;
			downBorderSkin = twitter_post_icon_down;
		}
	}
}