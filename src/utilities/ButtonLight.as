package utilities
{
	import flash.display.Sprite;
	
	import spark.components.supportClasses.StyleableTextField;
	
	public class ButtonLight extends Sprite
	{
		private var _id:String;
		private var _theText:StyleableTextField;
		
		
		public function ButtonLight()
		{
			super();
			
			_theText = new StyleableTextField()
			_theText.width = 260;
			_theText.setStyle("fontFamily", "myFontFamily");
			_theText.setStyle("fontSize", "28");
			_theText.multiline = true;
			_theText.wordWrap = true;
			addChild(_theText);
		}

		public function get label():String
		{
			return _theText.text;
		}

		public function set label(value:String):void
		{
			
			_theText.text = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

	}
}