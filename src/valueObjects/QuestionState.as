package valueObjects
{
	public class QuestionState
	{
		
		private var _question:String;
		private var _explanation:String;
		private var _answers:Array;
		private var _positionings:Array;
		private var _correctAnswer:int;
		private var _image:String;
		private var _selectedButtons:Array = [];
		
		
		public function QuestionState()
		{
		}

		public function get image():String
		{
			return _image;
		}

		public function set image(value:String):void
		{
			_image = value;
		}

		public function get selectedButtons():Array
		{
			return _selectedButtons;
		}

		public function set selectedButtons(value:Array):void
		{
			_selectedButtons = value;
		}

		public function get positionings():Array
		{
			return _positionings;
		}

		public function set positionings(value:Array):void
		{
			_positionings = value;
		}

		public function get correctAnswer():int
		{
			return _correctAnswer;
		}

		public function set correctAnswer(value:int):void
		{
			_correctAnswer = value;
			trace("_correctAnswer = "+_correctAnswer);
		}

		public function get answers():Array
		{
			return _answers;
		}

		public function set answers(value:Array):void
		{
			_answers = value;
			trace("_answers = "+_answers);
		}

		public function get explanation():String
		{
			return _explanation;
		}

		public function set explanation(value:String):void
		{
			_explanation = value;
			trace("_explanation = "+_explanation);
		}

		public function get question():String
		{
			return _question;
		}

		public function set question(value:String):void
		{
			_question = value;
			trace("_question = "+_question);
		}

	}
}