
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _QuestionDataEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("WRONG_ANS_3", "QUESTION", "EXPLANATION", "WRONG_ANS_1", "WRONG_ANS_2", "ANSWER", "SECTION");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("WRONG_ANS_3", "QUESTION", "EXPLANATION", "WRONG_ANS_1", "WRONG_ANS_2", "ANSWER", "SECTION");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("WRONG_ANS_3", "QUESTION", "EXPLANATION", "WRONG_ANS_1", "WRONG_ANS_2", "ANSWER", "SECTION");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("WRONG_ANS_3", "QUESTION", "EXPLANATION", "WRONG_ANS_1", "WRONG_ANS_2", "ANSWER", "SECTION");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("WRONG_ANS_3", "QUESTION", "EXPLANATION", "WRONG_ANS_1", "WRONG_ANS_2", "ANSWER", "SECTION");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "QuestionData";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _WRONG_ANS_3IsValid:Boolean;
    model_internal var _WRONG_ANS_3Validator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _WRONG_ANS_3IsValidCacheInitialized:Boolean = false;
    model_internal var _WRONG_ANS_3ValidationFailureMessages:Array;
    
    model_internal var _QUESTIONIsValid:Boolean;
    model_internal var _QUESTIONValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _QUESTIONIsValidCacheInitialized:Boolean = false;
    model_internal var _QUESTIONValidationFailureMessages:Array;
    
    model_internal var _EXPLANATIONIsValid:Boolean;
    model_internal var _EXPLANATIONValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _EXPLANATIONIsValidCacheInitialized:Boolean = false;
    model_internal var _EXPLANATIONValidationFailureMessages:Array;
    
    model_internal var _WRONG_ANS_1IsValid:Boolean;
    model_internal var _WRONG_ANS_1Validator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _WRONG_ANS_1IsValidCacheInitialized:Boolean = false;
    model_internal var _WRONG_ANS_1ValidationFailureMessages:Array;
    
    model_internal var _WRONG_ANS_2IsValid:Boolean;
    model_internal var _WRONG_ANS_2Validator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _WRONG_ANS_2IsValidCacheInitialized:Boolean = false;
    model_internal var _WRONG_ANS_2ValidationFailureMessages:Array;
    
    model_internal var _ANSWERIsValid:Boolean;
    model_internal var _ANSWERValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _ANSWERIsValidCacheInitialized:Boolean = false;
    model_internal var _ANSWERValidationFailureMessages:Array;
    
    model_internal var _SECTIONIsValid:Boolean;
    model_internal var _SECTIONValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _SECTIONIsValidCacheInitialized:Boolean = false;
    model_internal var _SECTIONValidationFailureMessages:Array;

    model_internal var _instance:_Super_QuestionData;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _QuestionDataEntityMetadata(value : _Super_QuestionData)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["WRONG_ANS_3"] = new Array();
            model_internal::dependentsOnMap["QUESTION"] = new Array();
            model_internal::dependentsOnMap["EXPLANATION"] = new Array();
            model_internal::dependentsOnMap["WRONG_ANS_1"] = new Array();
            model_internal::dependentsOnMap["WRONG_ANS_2"] = new Array();
            model_internal::dependentsOnMap["ANSWER"] = new Array();
            model_internal::dependentsOnMap["SECTION"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["WRONG_ANS_3"] = "String";
        model_internal::propertyTypeMap["QUESTION"] = "String";
        model_internal::propertyTypeMap["EXPLANATION"] = "String";
        model_internal::propertyTypeMap["WRONG_ANS_1"] = "String";
        model_internal::propertyTypeMap["WRONG_ANS_2"] = "String";
        model_internal::propertyTypeMap["ANSWER"] = "String";
        model_internal::propertyTypeMap["SECTION"] = "String";

        model_internal::_instance = value;
        model_internal::_WRONG_ANS_3Validator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForWRONG_ANS_3);
        model_internal::_WRONG_ANS_3Validator.required = true;
        model_internal::_WRONG_ANS_3Validator.requiredFieldError = "WRONG_ANS_3 is required";
        //model_internal::_WRONG_ANS_3Validator.source = model_internal::_instance;
        //model_internal::_WRONG_ANS_3Validator.property = "WRONG_ANS_3";
        model_internal::_QUESTIONValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForQUESTION);
        model_internal::_QUESTIONValidator.required = true;
        model_internal::_QUESTIONValidator.requiredFieldError = "QUESTION is required";
        //model_internal::_QUESTIONValidator.source = model_internal::_instance;
        //model_internal::_QUESTIONValidator.property = "QUESTION";
        model_internal::_EXPLANATIONValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForEXPLANATION);
        model_internal::_EXPLANATIONValidator.required = true;
        model_internal::_EXPLANATIONValidator.requiredFieldError = "EXPLANATION is required";
        //model_internal::_EXPLANATIONValidator.source = model_internal::_instance;
        //model_internal::_EXPLANATIONValidator.property = "EXPLANATION";
        model_internal::_WRONG_ANS_1Validator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForWRONG_ANS_1);
        model_internal::_WRONG_ANS_1Validator.required = true;
        model_internal::_WRONG_ANS_1Validator.requiredFieldError = "WRONG_ANS_1 is required";
        //model_internal::_WRONG_ANS_1Validator.source = model_internal::_instance;
        //model_internal::_WRONG_ANS_1Validator.property = "WRONG_ANS_1";
        model_internal::_WRONG_ANS_2Validator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForWRONG_ANS_2);
        model_internal::_WRONG_ANS_2Validator.required = true;
        model_internal::_WRONG_ANS_2Validator.requiredFieldError = "WRONG_ANS_2 is required";
        //model_internal::_WRONG_ANS_2Validator.source = model_internal::_instance;
        //model_internal::_WRONG_ANS_2Validator.property = "WRONG_ANS_2";
        model_internal::_ANSWERValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForANSWER);
        model_internal::_ANSWERValidator.required = true;
        model_internal::_ANSWERValidator.requiredFieldError = "ANSWER is required";
        //model_internal::_ANSWERValidator.source = model_internal::_instance;
        //model_internal::_ANSWERValidator.property = "ANSWER";
        model_internal::_SECTIONValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForSECTION);
        model_internal::_SECTIONValidator.required = true;
        model_internal::_SECTIONValidator.requiredFieldError = "SECTION is required";
        //model_internal::_SECTIONValidator.source = model_internal::_instance;
        //model_internal::_SECTIONValidator.property = "SECTION";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity QuestionData");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity QuestionData");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of QuestionData");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity QuestionData");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity QuestionData");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity QuestionData");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isWRONG_ANS_3Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isQUESTIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isEXPLANATIONAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWRONG_ANS_1Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isWRONG_ANS_2Available():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isANSWERAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSECTIONAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnWRONG_ANS_3():void
    {
        if (model_internal::_WRONG_ANS_3IsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfWRONG_ANS_3 = null;
            model_internal::calculateWRONG_ANS_3IsValid();
        }
    }
    public function invalidateDependentOnQUESTION():void
    {
        if (model_internal::_QUESTIONIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfQUESTION = null;
            model_internal::calculateQUESTIONIsValid();
        }
    }
    public function invalidateDependentOnEXPLANATION():void
    {
        if (model_internal::_EXPLANATIONIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfEXPLANATION = null;
            model_internal::calculateEXPLANATIONIsValid();
        }
    }
    public function invalidateDependentOnWRONG_ANS_1():void
    {
        if (model_internal::_WRONG_ANS_1IsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfWRONG_ANS_1 = null;
            model_internal::calculateWRONG_ANS_1IsValid();
        }
    }
    public function invalidateDependentOnWRONG_ANS_2():void
    {
        if (model_internal::_WRONG_ANS_2IsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfWRONG_ANS_2 = null;
            model_internal::calculateWRONG_ANS_2IsValid();
        }
    }
    public function invalidateDependentOnANSWER():void
    {
        if (model_internal::_ANSWERIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfANSWER = null;
            model_internal::calculateANSWERIsValid();
        }
    }
    public function invalidateDependentOnSECTION():void
    {
        if (model_internal::_SECTIONIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfSECTION = null;
            model_internal::calculateSECTIONIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get WRONG_ANS_3Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get WRONG_ANS_3Validator() : StyleValidator
    {
        return model_internal::_WRONG_ANS_3Validator;
    }

    model_internal function set _WRONG_ANS_3IsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_WRONG_ANS_3IsValid;         
        if (oldValue !== value)
        {
            model_internal::_WRONG_ANS_3IsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_3IsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_3IsValid():Boolean
    {
        if (!model_internal::_WRONG_ANS_3IsValidCacheInitialized)
        {
            model_internal::calculateWRONG_ANS_3IsValid();
        }

        return model_internal::_WRONG_ANS_3IsValid;
    }

    model_internal function calculateWRONG_ANS_3IsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_WRONG_ANS_3Validator.validate(model_internal::_instance.WRONG_ANS_3)
        model_internal::_WRONG_ANS_3IsValid_der = (valRes.results == null);
        model_internal::_WRONG_ANS_3IsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::WRONG_ANS_3ValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::WRONG_ANS_3ValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_3ValidationFailureMessages():Array
    {
        if (model_internal::_WRONG_ANS_3ValidationFailureMessages == null)
            model_internal::calculateWRONG_ANS_3IsValid();

        return _WRONG_ANS_3ValidationFailureMessages;
    }

    model_internal function set WRONG_ANS_3ValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_WRONG_ANS_3ValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_WRONG_ANS_3ValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_3ValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get QUESTIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get QUESTIONValidator() : StyleValidator
    {
        return model_internal::_QUESTIONValidator;
    }

    model_internal function set _QUESTIONIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_QUESTIONIsValid;         
        if (oldValue !== value)
        {
            model_internal::_QUESTIONIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "QUESTIONIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get QUESTIONIsValid():Boolean
    {
        if (!model_internal::_QUESTIONIsValidCacheInitialized)
        {
            model_internal::calculateQUESTIONIsValid();
        }

        return model_internal::_QUESTIONIsValid;
    }

    model_internal function calculateQUESTIONIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_QUESTIONValidator.validate(model_internal::_instance.QUESTION)
        model_internal::_QUESTIONIsValid_der = (valRes.results == null);
        model_internal::_QUESTIONIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::QUESTIONValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::QUESTIONValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get QUESTIONValidationFailureMessages():Array
    {
        if (model_internal::_QUESTIONValidationFailureMessages == null)
            model_internal::calculateQUESTIONIsValid();

        return _QUESTIONValidationFailureMessages;
    }

    model_internal function set QUESTIONValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_QUESTIONValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_QUESTIONValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "QUESTIONValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get EXPLANATIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get EXPLANATIONValidator() : StyleValidator
    {
        return model_internal::_EXPLANATIONValidator;
    }

    model_internal function set _EXPLANATIONIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_EXPLANATIONIsValid;         
        if (oldValue !== value)
        {
            model_internal::_EXPLANATIONIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXPLANATIONIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get EXPLANATIONIsValid():Boolean
    {
        if (!model_internal::_EXPLANATIONIsValidCacheInitialized)
        {
            model_internal::calculateEXPLANATIONIsValid();
        }

        return model_internal::_EXPLANATIONIsValid;
    }

    model_internal function calculateEXPLANATIONIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_EXPLANATIONValidator.validate(model_internal::_instance.EXPLANATION)
        model_internal::_EXPLANATIONIsValid_der = (valRes.results == null);
        model_internal::_EXPLANATIONIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::EXPLANATIONValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::EXPLANATIONValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get EXPLANATIONValidationFailureMessages():Array
    {
        if (model_internal::_EXPLANATIONValidationFailureMessages == null)
            model_internal::calculateEXPLANATIONIsValid();

        return _EXPLANATIONValidationFailureMessages;
    }

    model_internal function set EXPLANATIONValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_EXPLANATIONValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_EXPLANATIONValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXPLANATIONValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get WRONG_ANS_1Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get WRONG_ANS_1Validator() : StyleValidator
    {
        return model_internal::_WRONG_ANS_1Validator;
    }

    model_internal function set _WRONG_ANS_1IsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_WRONG_ANS_1IsValid;         
        if (oldValue !== value)
        {
            model_internal::_WRONG_ANS_1IsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_1IsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_1IsValid():Boolean
    {
        if (!model_internal::_WRONG_ANS_1IsValidCacheInitialized)
        {
            model_internal::calculateWRONG_ANS_1IsValid();
        }

        return model_internal::_WRONG_ANS_1IsValid;
    }

    model_internal function calculateWRONG_ANS_1IsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_WRONG_ANS_1Validator.validate(model_internal::_instance.WRONG_ANS_1)
        model_internal::_WRONG_ANS_1IsValid_der = (valRes.results == null);
        model_internal::_WRONG_ANS_1IsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::WRONG_ANS_1ValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::WRONG_ANS_1ValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_1ValidationFailureMessages():Array
    {
        if (model_internal::_WRONG_ANS_1ValidationFailureMessages == null)
            model_internal::calculateWRONG_ANS_1IsValid();

        return _WRONG_ANS_1ValidationFailureMessages;
    }

    model_internal function set WRONG_ANS_1ValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_WRONG_ANS_1ValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_WRONG_ANS_1ValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_1ValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get WRONG_ANS_2Style():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get WRONG_ANS_2Validator() : StyleValidator
    {
        return model_internal::_WRONG_ANS_2Validator;
    }

    model_internal function set _WRONG_ANS_2IsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_WRONG_ANS_2IsValid;         
        if (oldValue !== value)
        {
            model_internal::_WRONG_ANS_2IsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_2IsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_2IsValid():Boolean
    {
        if (!model_internal::_WRONG_ANS_2IsValidCacheInitialized)
        {
            model_internal::calculateWRONG_ANS_2IsValid();
        }

        return model_internal::_WRONG_ANS_2IsValid;
    }

    model_internal function calculateWRONG_ANS_2IsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_WRONG_ANS_2Validator.validate(model_internal::_instance.WRONG_ANS_2)
        model_internal::_WRONG_ANS_2IsValid_der = (valRes.results == null);
        model_internal::_WRONG_ANS_2IsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::WRONG_ANS_2ValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::WRONG_ANS_2ValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_2ValidationFailureMessages():Array
    {
        if (model_internal::_WRONG_ANS_2ValidationFailureMessages == null)
            model_internal::calculateWRONG_ANS_2IsValid();

        return _WRONG_ANS_2ValidationFailureMessages;
    }

    model_internal function set WRONG_ANS_2ValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_WRONG_ANS_2ValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_WRONG_ANS_2ValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_2ValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get ANSWERStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get ANSWERValidator() : StyleValidator
    {
        return model_internal::_ANSWERValidator;
    }

    model_internal function set _ANSWERIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_ANSWERIsValid;         
        if (oldValue !== value)
        {
            model_internal::_ANSWERIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ANSWERIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get ANSWERIsValid():Boolean
    {
        if (!model_internal::_ANSWERIsValidCacheInitialized)
        {
            model_internal::calculateANSWERIsValid();
        }

        return model_internal::_ANSWERIsValid;
    }

    model_internal function calculateANSWERIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_ANSWERValidator.validate(model_internal::_instance.ANSWER)
        model_internal::_ANSWERIsValid_der = (valRes.results == null);
        model_internal::_ANSWERIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::ANSWERValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::ANSWERValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get ANSWERValidationFailureMessages():Array
    {
        if (model_internal::_ANSWERValidationFailureMessages == null)
            model_internal::calculateANSWERIsValid();

        return _ANSWERValidationFailureMessages;
    }

    model_internal function set ANSWERValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_ANSWERValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_ANSWERValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ANSWERValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get SECTIONStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get SECTIONValidator() : StyleValidator
    {
        return model_internal::_SECTIONValidator;
    }

    model_internal function set _SECTIONIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_SECTIONIsValid;         
        if (oldValue !== value)
        {
            model_internal::_SECTIONIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SECTIONIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get SECTIONIsValid():Boolean
    {
        if (!model_internal::_SECTIONIsValidCacheInitialized)
        {
            model_internal::calculateSECTIONIsValid();
        }

        return model_internal::_SECTIONIsValid;
    }

    model_internal function calculateSECTIONIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_SECTIONValidator.validate(model_internal::_instance.SECTION)
        model_internal::_SECTIONIsValid_der = (valRes.results == null);
        model_internal::_SECTIONIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::SECTIONValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::SECTIONValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get SECTIONValidationFailureMessages():Array
    {
        if (model_internal::_SECTIONValidationFailureMessages == null)
            model_internal::calculateSECTIONIsValid();

        return _SECTIONValidationFailureMessages;
    }

    model_internal function set SECTIONValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_SECTIONValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_SECTIONValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SECTIONValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("WRONG_ANS_3"):
            {
                return WRONG_ANS_3ValidationFailureMessages;
            }
            case("QUESTION"):
            {
                return QUESTIONValidationFailureMessages;
            }
            case("EXPLANATION"):
            {
                return EXPLANATIONValidationFailureMessages;
            }
            case("WRONG_ANS_1"):
            {
                return WRONG_ANS_1ValidationFailureMessages;
            }
            case("WRONG_ANS_2"):
            {
                return WRONG_ANS_2ValidationFailureMessages;
            }
            case("ANSWER"):
            {
                return ANSWERValidationFailureMessages;
            }
            case("SECTION"):
            {
                return SECTIONValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
