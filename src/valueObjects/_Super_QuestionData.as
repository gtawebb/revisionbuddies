/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - QuestionData.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_QuestionData extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _QuestionDataEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_WRONG_ANS_3 : String;
    private var _internal_QUESTION : String;
    private var _internal_EXPLANATION : String;
    private var _internal_WRONG_ANS_1 : String;
    private var _internal_WRONG_ANS_2 : String;
    private var _internal_ANSWER : String;
    private var _internal_SECTION : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_QuestionData()
    {
        _model = new _QuestionDataEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "WRONG_ANS_3", model_internal::setterListenerWRONG_ANS_3));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "QUESTION", model_internal::setterListenerQUESTION));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "EXPLANATION", model_internal::setterListenerEXPLANATION));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "WRONG_ANS_1", model_internal::setterListenerWRONG_ANS_1));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "WRONG_ANS_2", model_internal::setterListenerWRONG_ANS_2));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "ANSWER", model_internal::setterListenerANSWER));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "SECTION", model_internal::setterListenerSECTION));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_3() : String
    {
        return _internal_WRONG_ANS_3;
    }

    [Bindable(event="propertyChange")]
    public function get QUESTION() : String
    {
        return _internal_QUESTION;
    }

    [Bindable(event="propertyChange")]
    public function get EXPLANATION() : String
    {
        return _internal_EXPLANATION;
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_1() : String
    {
        return _internal_WRONG_ANS_1;
    }

    [Bindable(event="propertyChange")]
    public function get WRONG_ANS_2() : String
    {
        return _internal_WRONG_ANS_2;
    }

    [Bindable(event="propertyChange")]
    public function get ANSWER() : String
    {
        return _internal_ANSWER;
    }

    [Bindable(event="propertyChange")]
    public function get SECTION() : String
    {
        return _internal_SECTION;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set WRONG_ANS_3(value:String) : void
    {
        var oldValue:String = _internal_WRONG_ANS_3;
        if (oldValue !== value)
        {
            _internal_WRONG_ANS_3 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_3", oldValue, _internal_WRONG_ANS_3));
        }
    }

    public function set QUESTION(value:String) : void
    {
        var oldValue:String = _internal_QUESTION;
        if (oldValue !== value)
        {
            _internal_QUESTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "QUESTION", oldValue, _internal_QUESTION));
        }
    }

    public function set EXPLANATION(value:String) : void
    {
        var oldValue:String = _internal_EXPLANATION;
        if (oldValue !== value)
        {
            _internal_EXPLANATION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "EXPLANATION", oldValue, _internal_EXPLANATION));
        }
    }

    public function set WRONG_ANS_1(value:String) : void
    {
        var oldValue:String = _internal_WRONG_ANS_1;
        if (oldValue !== value)
        {
            _internal_WRONG_ANS_1 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_1", oldValue, _internal_WRONG_ANS_1));
        }
    }

    public function set WRONG_ANS_2(value:String) : void
    {
        var oldValue:String = _internal_WRONG_ANS_2;
        if (oldValue !== value)
        {
            _internal_WRONG_ANS_2 = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "WRONG_ANS_2", oldValue, _internal_WRONG_ANS_2));
        }
    }

    public function set ANSWER(value:String) : void
    {
        var oldValue:String = _internal_ANSWER;
        if (oldValue !== value)
        {
            _internal_ANSWER = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "ANSWER", oldValue, _internal_ANSWER));
        }
    }

    public function set SECTION(value:String) : void
    {
        var oldValue:String = _internal_SECTION;
        if (oldValue !== value)
        {
            _internal_SECTION = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "SECTION", oldValue, _internal_SECTION));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerWRONG_ANS_3(value:flash.events.Event):void
    {
        _model.invalidateDependentOnWRONG_ANS_3();
    }

    model_internal function setterListenerQUESTION(value:flash.events.Event):void
    {
        _model.invalidateDependentOnQUESTION();
    }

    model_internal function setterListenerEXPLANATION(value:flash.events.Event):void
    {
        _model.invalidateDependentOnEXPLANATION();
    }

    model_internal function setterListenerWRONG_ANS_1(value:flash.events.Event):void
    {
        _model.invalidateDependentOnWRONG_ANS_1();
    }

    model_internal function setterListenerWRONG_ANS_2(value:flash.events.Event):void
    {
        _model.invalidateDependentOnWRONG_ANS_2();
    }

    model_internal function setterListenerANSWER(value:flash.events.Event):void
    {
        _model.invalidateDependentOnANSWER();
    }

    model_internal function setterListenerSECTION(value:flash.events.Event):void
    {
        _model.invalidateDependentOnSECTION();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.WRONG_ANS_3IsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_WRONG_ANS_3ValidationFailureMessages);
        }
        if (!_model.QUESTIONIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_QUESTIONValidationFailureMessages);
        }
        if (!_model.EXPLANATIONIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_EXPLANATIONValidationFailureMessages);
        }
        if (!_model.WRONG_ANS_1IsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_WRONG_ANS_1ValidationFailureMessages);
        }
        if (!_model.WRONG_ANS_2IsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_WRONG_ANS_2ValidationFailureMessages);
        }
        if (!_model.ANSWERIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_ANSWERValidationFailureMessages);
        }
        if (!_model.SECTIONIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_SECTIONValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _QuestionDataEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _QuestionDataEntityMetadata) : void
    {
        var oldValue : _QuestionDataEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfWRONG_ANS_3 : Array = null;
    model_internal var _doValidationLastValOfWRONG_ANS_3 : String;

    model_internal function _doValidationForWRONG_ANS_3(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfWRONG_ANS_3 != null && model_internal::_doValidationLastValOfWRONG_ANS_3 == value)
           return model_internal::_doValidationCacheOfWRONG_ANS_3 ;

        _model.model_internal::_WRONG_ANS_3IsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isWRONG_ANS_3Available && _internal_WRONG_ANS_3 == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "WRONG_ANS_3 is required"));
        }

        model_internal::_doValidationCacheOfWRONG_ANS_3 = validationFailures;
        model_internal::_doValidationLastValOfWRONG_ANS_3 = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfQUESTION : Array = null;
    model_internal var _doValidationLastValOfQUESTION : String;

    model_internal function _doValidationForQUESTION(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfQUESTION != null && model_internal::_doValidationLastValOfQUESTION == value)
           return model_internal::_doValidationCacheOfQUESTION ;

        _model.model_internal::_QUESTIONIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isQUESTIONAvailable && _internal_QUESTION == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "QUESTION is required"));
        }

        model_internal::_doValidationCacheOfQUESTION = validationFailures;
        model_internal::_doValidationLastValOfQUESTION = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfEXPLANATION : Array = null;
    model_internal var _doValidationLastValOfEXPLANATION : String;

    model_internal function _doValidationForEXPLANATION(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfEXPLANATION != null && model_internal::_doValidationLastValOfEXPLANATION == value)
           return model_internal::_doValidationCacheOfEXPLANATION ;

        _model.model_internal::_EXPLANATIONIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isEXPLANATIONAvailable && _internal_EXPLANATION == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "EXPLANATION is required"));
        }

        model_internal::_doValidationCacheOfEXPLANATION = validationFailures;
        model_internal::_doValidationLastValOfEXPLANATION = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfWRONG_ANS_1 : Array = null;
    model_internal var _doValidationLastValOfWRONG_ANS_1 : String;

    model_internal function _doValidationForWRONG_ANS_1(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfWRONG_ANS_1 != null && model_internal::_doValidationLastValOfWRONG_ANS_1 == value)
           return model_internal::_doValidationCacheOfWRONG_ANS_1 ;

        _model.model_internal::_WRONG_ANS_1IsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isWRONG_ANS_1Available && _internal_WRONG_ANS_1 == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "WRONG_ANS_1 is required"));
        }

        model_internal::_doValidationCacheOfWRONG_ANS_1 = validationFailures;
        model_internal::_doValidationLastValOfWRONG_ANS_1 = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfWRONG_ANS_2 : Array = null;
    model_internal var _doValidationLastValOfWRONG_ANS_2 : String;

    model_internal function _doValidationForWRONG_ANS_2(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfWRONG_ANS_2 != null && model_internal::_doValidationLastValOfWRONG_ANS_2 == value)
           return model_internal::_doValidationCacheOfWRONG_ANS_2 ;

        _model.model_internal::_WRONG_ANS_2IsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isWRONG_ANS_2Available && _internal_WRONG_ANS_2 == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "WRONG_ANS_2 is required"));
        }

        model_internal::_doValidationCacheOfWRONG_ANS_2 = validationFailures;
        model_internal::_doValidationLastValOfWRONG_ANS_2 = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfANSWER : Array = null;
    model_internal var _doValidationLastValOfANSWER : String;

    model_internal function _doValidationForANSWER(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfANSWER != null && model_internal::_doValidationLastValOfANSWER == value)
           return model_internal::_doValidationCacheOfANSWER ;

        _model.model_internal::_ANSWERIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isANSWERAvailable && _internal_ANSWER == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "ANSWER is required"));
        }

        model_internal::_doValidationCacheOfANSWER = validationFailures;
        model_internal::_doValidationLastValOfANSWER = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfSECTION : Array = null;
    model_internal var _doValidationLastValOfSECTION : String;

    model_internal function _doValidationForSECTION(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfSECTION != null && model_internal::_doValidationLastValOfSECTION == value)
           return model_internal::_doValidationCacheOfSECTION ;

        _model.model_internal::_SECTIONIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isSECTIONAvailable && _internal_SECTION == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "SECTION is required"));
        }

        model_internal::_doValidationCacheOfSECTION = validationFailures;
        model_internal::_doValidationLastValOfSECTION = value;

        return validationFailures;
    }
    

}

}
